# Game Tech

## TL;DR

- Build on x64
- Single player - Start up project: Client
- Multiplayer   - Start up Project: Server + Client
- Controls      - Right Click + WASD to Move camera, left click to push, scroll change force

## Building

The program uses the Windows 10.0.17763.0 SDK (although others should also work).

It's also 64bit (x64) and should be able to compiled in debug and release.

Also best with Vsync on....

## Single Player

To build the single player version, make the "Client" project the start up project and run that.

## Multiplayer

To test the multiplayer version, have multiple startup projects for visual studio. The first should be "Server" and the 2nd one should be "Client". Once launched, the client has to select the multiplayer option (option 2)  before the server starts to join the server.

Once the game has started, the Client should be able to move the ball, and the server should only provide visuals.


# Controls

Right Click + WASD - Move camera
Left click      - Push Ball
Scroll Wheel    - Add/Minus push force,
WASD            - move camera
Space           - Move up
C               - Move Down
R               - Reset Level
F1              - Debug mode (Default tutorial controls)