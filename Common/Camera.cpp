#include "Camera.h"
#include "Window.h"
#include <algorithm>
#include "Maths.h"

using namespace NCL;

/*
Polls the camera for keyboard / mouse movement.
Should be done once per frame! Pass it the msec since
last frame (default value is for simplicities sake...)
*/
void Camera::UpdateCamera(float dt) {
	//Update the mouse by how much
	m_pitch	-= (Window::GetMouse()->GetRelativePosition().y);
	m_yaw		-= (Window::GetMouse()->GetRelativePosition().x);

	//Bounds check the pitch, to be between straight up and straight down ;)
	m_pitch = std::min(m_pitch, 90.0f);
	m_pitch = std::max(m_pitch, -90.0f);

	if (m_yaw <0) {
		m_yaw += 360.0f;
	}
	if (m_yaw > 360.0f) {
		m_yaw -= 360.0f;
	}

	float frameSpeed = 400 * dt;

	if (Window::GetKeyboard()->KeyDown(KEYBOARD_W)) {
		m_position += Matrix4::Rotation(m_yaw, Vector3(0, 1, 0)) * Vector3(0, 0, -1) * frameSpeed;
	}
	if (Window::GetKeyboard()->KeyDown(KEYBOARD_S)) {
		m_position -= Matrix4::Rotation(m_yaw, Vector3(0, 1, 0)) * Vector3(0, 0, -1) * frameSpeed;
	}

	if (Window::GetKeyboard()->KeyDown(KEYBOARD_A)) {
		m_position += Matrix4::Rotation(m_yaw, Vector3(0, 1, 0)) * Vector3(-1, 0, 0) * frameSpeed;
	}
	if (Window::GetKeyboard()->KeyDown(KEYBOARD_D)) {
		m_position -= Matrix4::Rotation(m_yaw, Vector3(0, 1, 0)) * Vector3(-1, 0, 0) * frameSpeed;
	}

	if (Window::GetKeyboard()->KeyDown(KEYBOARD_SPACE)) {
		m_position.y += frameSpeed;
	}
	if (Window::GetKeyboard()->KeyDown(KEYBOARD_C)) {
		m_position.y -= frameSpeed;
	}


}



/*
Generates a view matrix for the camera's viewpoint. This matrix can be sent
straight to the shader...it's already an 'inverse camera' matrix.
*/
Matrix4 Camera::BuildViewMatrix() const {
	//Why do a complicated matrix inversion, when we can just generate the matrix
	//using the negative values ;). The matrix multiplication order is important!
	return	Matrix4::Rotation(-m_pitch, Vector3(1, 0, 0)) *
		Matrix4::Rotation(-m_yaw, Vector3(0, 1, 0)) *
		Matrix4::Translation(-m_position);
};

Matrix4 Camera::BuildProjectionMatrix(float currentAspect) const {
	if (camType == CameraType::Orthographic) {
		return Matrix4::Orthographic(m_nearPlane, m_farPlane, m_right, m_left, m_top, m_bottom);
	}
	//else if (camType == CameraType::Perspective) {
		return Matrix4::Perspective(m_nearPlane, m_farPlane, currentAspect, m_fov);
	//}
}

Camera Camera::BuildPerspectiveCamera(const Vector3& pos, float pitch, float yaw, float fov, float nearPlane, float farPlane) {
	Camera c;
	c.camType	= CameraType::Perspective;
	c.m_position	= pos;
	c.m_pitch		= pitch;
	c.m_yaw		= yaw;
	c.m_nearPlane = nearPlane;
	c.m_farPlane  = farPlane;

	c.m_fov		= fov;

	return c;
}
Camera Camera::BuildOrthoCamera(const Vector3& pos, float pitch, float yaw, float left, float right, float top, float bottom, float nearPlane, float farPlane) {
	Camera c;
	c.camType	= CameraType::Orthographic;
	c.m_position	= pos;
	c.m_pitch		= pitch;
	c.m_yaw		= yaw;
	c.m_nearPlane = nearPlane;
	c.m_farPlane	= farPlane;

	c.m_left		= left;
	c.m_right		= right;
	c.m_top		= top;
	c.m_bottom	= bottom;

	return c;
}

void Camera::followPos(Vector3 pos) {

	float camX = m_distFromPlayer * -sinf(m_yaw*(PI / 180)) * cosf((m_pitch)*(PI / 180));
	float camY = m_distFromPlayer * -sinf((m_pitch)*(PI / 180));
	float camZ = m_distFromPlayer * cosf((m_yaw)*(PI / 180)) * cosf((m_pitch)*(PI / 180));

	m_position = Vector3(camX, camY, camZ);
	//lookAt(Vector3(0,0,0));



}

void Camera::lookAt(Vector3 pos)
{
	Matrix4 lookat = Matrix4::BuildViewMatrix(m_position, pos, Vector3(0, 1, 0));
	float roll = 0;
	lookat.GetRotation(m_yaw, m_pitch, roll);
}