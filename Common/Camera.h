#pragma once
#include "Matrix4.h"
#include "Vector3.h"

namespace NCL {
	using namespace NCL::Maths;
	enum CameraType {
		Orthographic,
		Perspective
	};

	class Camera {
	public:
		Camera(void) {
			m_pitch		= 0.0f;
			m_yaw			= 0.0f;

			this->m_fov	= 45.0f;
			this->m_nearPlane = 1.0f;
			this->m_farPlane	= 100.0f;

			this->camType = CameraType::Perspective;
			m_shouldFollow = Vector3(0, 0, 0);

		};

		Camera(float pitch, float yaw, const Vector3& position) {
			this->m_pitch		= pitch;
			this->m_yaw		= yaw;
			this->m_position	= position;

			this->m_fov		= 45.0f;
			this->m_nearPlane = 1.0f;
			this->m_farPlane	= 100.0f;

			this->camType	= CameraType::Perspective;

			m_shouldFollow = Vector3(0, 0, 0);

		}

		~Camera(void) {};

		void UpdateCamera(float dt);


		float GetFieldOfVision() const {
			return m_fov;
		}

		float GetNearPlane() const {
			return m_nearPlane;
		}

		float GetFarPlane() const {
			return m_farPlane;
		}

		void SetNearPlane(float val) {
			m_nearPlane = val;
		}
		
		void SetFarPlane(float val) {
			m_farPlane = val;
		}

		//Builds a view matrix for the current camera variables, suitable for sending straight
		//to a vertex shader (i.e it's already an 'inverse camera matrix').
		Matrix4 BuildViewMatrix() const;

		Matrix4 BuildProjectionMatrix(float currentAspect = 1.0f) const;

		//Gets position in world space
		Vector3 GetPosition() const { return m_position; }
		//Sets position in world space
		void	SetPosition(const Vector3& val) { m_position = val; }

		//Gets yaw, in degrees
		float	GetYaw()   const { return m_yaw; }
		//Sets yaw, in degrees
		void	SetYaw(float y) { m_yaw = y; }

		//Gets pitch, in degrees
		float	GetPitch() const { return m_pitch; }
		//Sets pitch, in degrees
		void	SetPitch(float p) { m_pitch = p; }

		static Camera BuildPerspectiveCamera(const Vector3& pos, float pitch, float yaw, float fov, float near, float far);
		static Camera BuildOrthoCamera(const Vector3& pos, float pitch, float yaw, float left, float right, float top, float bottom, float near, float far);


		void setFollow(Vector3 f) { m_shouldFollow = f; };

		void followPos(Vector3 pos);


	protected:
		CameraType camType;

	

		float	m_nearPlane;
		float	m_farPlane;
		float	m_left;
		float	m_right;
		float	m_top;
		float	m_bottom;

		float	m_fov;
		float	m_yaw;
		float	m_pitch;
		Vector3 m_position;


		// ----- 3rd person functions----- //

		void lookAt(Vector3 pos);
		Vector3 m_shouldFollow;
		float m_distFromPlayer = 5;
	};
}