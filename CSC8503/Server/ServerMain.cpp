#include <iostream>
#include "../CSC8503Common/NetworkBase.h"
#include "../CSC8503Common/GameServer.h"
#include <string>
#include "../CSC8503Common/PacketReceivers.h"
using namespace NCL::CSC8503;
using namespace std;

void BuildServer() {

	std::cout << "Starting Server! " << std::endl;

	
	// ----- Creating a window ----- //
	Window*w = Window::CreateGameWindow("Server Window", 1280, 720,false, 1000,100);

	if (!w->HasInitialised()) { return; }

	w->ShowOSPointer(false);
	w->LockMouseToWindow(true);

	// ----- Making a server  ----- //

	NetworkBase::Initialise();
	int port = NetworkBase::GetDefaultPort();
	GameServer* server = new GameServer(port, 4);

	std::cout << "Server Built! " << std::endl;


	bool exit = false;

	while (w->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		float dt = w->GetTimer()->GetTimeDelta() / 1000.0f;

		server->updateGame(dt);

		server->UpdateServer();
	}

	NetworkBase::Destroy();
}

int main() {
	BuildServer();

}