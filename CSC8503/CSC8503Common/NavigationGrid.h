#pragma once
#include "NavigationMap.h"
#include <string>
#include "../GameTech/FileReader.h"
#include "GameObject.h"
#include <queue>

namespace NCL {
	namespace CSC8503 {
		struct GridNode {
			GridNode* parent;

			GridNode* connected[4];
			int		  costs[4];

			Vector3		position;

			float f;
			float g;

			bool visited;

			char type;

			/*bool operator<(const GridNode& rhs) const {
				return f < rhs.f;
			}*/

			

			GridNode() {
				for (int i = 0; i < 4; ++i) {
					connected[i] = nullptr;
					costs[i] = 0; //traversal cost
				}
				f = 0;
				g = 0;
				visited = false;
				type = 0;
				parent = nullptr;
			}
			~GridNode() {	}

			// add comparable function here

		};

		struct CompareGridNode {

			bool operator()(const GridNode* lhs, const GridNode* rhs){
				return lhs->f > rhs->f;
			}
		};

		class NavigationGrid : public NavigationMap	{
		public:
			NavigationGrid(int nodeSize);
			NavigationGrid(const std::string&filename, int nodeSize);
			~NavigationGrid();

			bool FindPath(const Vector3& from, const Vector3& to, NavigationPath& outPath) override;
				
		protected:

			FileReader  m_fileReader;

			/// checks if a node is in the passed in list
			bool		NodeInList(GridNode* n, std::vector<GridNode*>& list) const;

			void ResetNodes();

			/// Finds and removes the lowest cost (best) node from 
			/// the passes in list of nodes.
			GridNode*	RemoveBestNode(std::vector<GridNode*>& list) const;
			
			/// Finds the heuristic between to nodes by calculating the
			/// length between them.
			float		Heuristic(GridNode* hNode, GridNode* endNode) const;
			int m_nodeSize;
			int m_gridWidth;
			int m_gridHeight;

			std::vector<GridNode*> m_closedList;
			std::priority_queue<GridNode*, std::vector<GridNode*>, CompareGridNode> m_openList;


			GridNode* m_allNodes;
		};
	}
}

