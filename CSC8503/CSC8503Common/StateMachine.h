#pragma once
#include <vector>
#include <map>

namespace NCL {
	namespace CSC8503 {

		class State;
		class StateTransition;

		typedef std::multimap<State*, StateTransition*> TransitionContainer;
		typedef TransitionContainer::iterator TransitionIterator;

		class StateMachine	{
		public:
			StateMachine();
			~StateMachine();

			void AddState(State* s);
			void AddTransition(StateTransition* t);

			void Update();
			StateTransition * getLastTransition() { return  m_lastTransition; };
			void clearLastTransition() { m_lastTransition = nullptr; }
		protected:
			State * m_activeState;

			std::vector<State*> m_allStates;

			StateTransition* m_lastTransition;
			TransitionContainer m_allTransitions;
		};
	}
}