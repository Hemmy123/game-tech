#include "ObjectNavigator.h"



ObjectNavigator::ObjectNavigator()
{
	m_vectorPathIndex = 0;
	m_gameObject = nullptr;

}


ObjectNavigator::~ObjectNavigator()
{
}

void ObjectNavigator::update() {
	if (m_vectorPathIndex >= m_vectorPath.size()) {

		return;
	}

	if(m_gameObject && m_vectorPath.size() != 0) {
		Vector3 currentPos = m_gameObject->GetTransform().GetWorldPosition();
		m_currentDestination = m_vectorPath[m_vectorPathIndex];
		m_currentDestination.y = 100;
		if (isAt(currentPos, m_currentDestination)) {
			m_vectorPathIndex++;

			
		}
		else {
			Vector3 direction = (m_currentDestination - m_gameObject->GetTransform().GetWorldPosition()).Normalised();
			float magnitude = speed;
			m_gameObject->GetPhysicsObject()->AddForce(direction * magnitude);


		}
	}
	}
	

void ObjectNavigator::setPath(std::vector<Vector3> path) {
	m_vectorPath = path;
}

bool ObjectNavigator::isAt(Vector3 a, Vector3 b) {
	return (abs(a.Length() - b.Length()) < 0.02);
}
