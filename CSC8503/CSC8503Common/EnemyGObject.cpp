#include "EnemyGObject.h"



EnemyGObject::EnemyGObject()
{
}


EnemyGObject::~EnemyGObject()
{
}

void EnemyGObject::OnCollisionBegin(GameObject* otherObject) {

	GameObjectType type = otherObject->GetType();

	switch (type) {
	case GameObjectType::Player: {
		Vector3 force = otherObject->GetPhysicsObject()->GetLinearVelocity();
		if(force.Length() > 70) {
			m_transform.SetLocalPosition(m_originalPosition);
		}
	}
	}

}
