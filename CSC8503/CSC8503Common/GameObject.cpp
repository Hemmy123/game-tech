#include "GameObject.h"
#include "CollisionDetection.h"

using namespace NCL::CSC8503;

GameObject::GameObject(string objectName)	{
	m_name			= objectName;
	m_isActive		= true;
	m_boundingVolume	= nullptr;
	m_physicsObject	= nullptr;
	m_renderObject	= nullptr;
	m_networkObject	= nullptr;
}

GameObject::~GameObject()	{
	delete m_boundingVolume;
	delete m_physicsObject;
	delete m_renderObject;
	delete m_networkObject;
}

bool GameObject::InsideAABB(const Vector3& boxPos, const Vector3& halfSize) {
	if (!m_boundingVolume) {
		return false;
	}
	return CollisionDetection::AABBTest(m_transform, *m_boundingVolume, boxPos, halfSize);
}

void GameObject::CollidingWith(GameObject* a, GameObject* b)
{
}

bool GameObject::GetBroadphaseAABB(Vector3& outSize) const {

	if(!m_boundingVolume) {
		return false;
	}

	outSize = m_broadphaseAABB;

	return true;
}

void GameObject::UpdateBroadphaseAABB() {
	if(!m_boundingVolume) {
		return;
	}

	if(m_boundingVolume->type == VolumeType::AABB) {
		m_broadphaseAABB = ((AABBVolume&)m_boundingVolume).GetHalfDimensions();
	}

	else if(m_boundingVolume->type == VolumeType::Sphere) {
		float r = ((SphereVolume&)*m_boundingVolume).GetRadius();
		m_broadphaseAABB = Vector3(r, r, r);
	}

	else if (m_boundingVolume->type == VolumeType::OBB) {
		Matrix3 mat = m_transform.GetWorldOrientation().ToMatrix3();
		mat = mat.Absolute();
		Vector3 halfSize =
			((OBBVolume&)*m_boundingVolume).GetHalfDimensions();
		m_broadphaseAABB = mat * halfSize;
	}


}
