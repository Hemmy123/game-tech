#include "PlayerGObject.h"



PlayerGObject::PlayerGObject()
{
	m_alive = true;
	m_reachedGoal = false;

}


PlayerGObject::~PlayerGObject()
{
}

void PlayerGObject::OnCollisionBegin(GameObject* otherObject) {

	GameObjectType type = otherObject->GetType();

	switch(type) {
	case GameObjectType::Goal: {
		m_reachedGoal = true;
		break;
	}
	case GameObjectType::Enemy: {
		m_alive = false;
		break;
	}
	

	}



}



