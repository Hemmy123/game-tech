#include "PhysicsObject.h"
#include "PhysicsSystem.h"
#include "../CSC8503Common/Transform.h"
using namespace NCL;
using namespace CSC8503;

PhysicsObject::PhysicsObject(Transform* parentTransform, const CollisionVolume* parentVolume)	{
	m_transform		= parentTransform;
	m_volume		= parentVolume;

	m_inverseMass	= 1.0f;
	m_elasticity	= 0.8f;
	m_friction		= 0.2f;
	m_atRest		= false;
	m_cumulativePos = Vector3();
	m_restTime = 0;
}

PhysicsObject::~PhysicsObject()	{

}

void PhysicsObject::updateRest(float dt) {


	// if not it haven't been moving and there's no
	// forces on it...
	if(	m_linearVelocity.Length() < 2 &&
		m_angularVelocity.Length() < 2 &&
		m_force.Length() < 5) {

		m_restTime += dt;

		if(m_restTime > 0.1) {
			m_linearVelocity	= Vector3();
			m_angularVelocity	= Vector3();
			m_force				= Vector3();

			m_atRest = true;
		}
	}

	if(m_force.Length() > 0.2 ) {
		m_atRest = false;
		m_restTime = 0;
	}




}

void PhysicsObject::ApplyAngularImpulse(const Vector3& force) {
	m_angularVelocity += m_inverseInteriaTensor * force;
}

void PhysicsObject::ApplyLinearImpulse(const Vector3& force) {
	m_linearVelocity += force * m_inverseMass;
}

void PhysicsObject::AddForce(const Vector3& addedForce) {
	m_force += addedForce;
}

void PhysicsObject::AddForceAtPosition(const Vector3& addedForce, const Vector3& position) {
	Vector3 localPos = m_transform->GetWorldPosition() - position;

	m_force += addedForce * PhysicsSystem::UNIT_MULTIPLIER;
	m_torque += Vector3::Cross(addedForce, localPos);
}

void PhysicsObject::AddTorque(const Vector3& addedTorque) {
	m_torque += addedTorque;
}

void PhysicsObject::ClearForces() {
	m_force		= Vector3();
	m_torque		= Vector3();
}

void PhysicsObject::InitCubeInertia() {
	//Vector3 dimensions	= transform->GetLocalScale();
	Vector3 dimensions	= m_transform->GetLocalScale() * 2;
	Vector3 dimsSqr		= dimensions * dimensions;

	m_inverseInertia.x = (12.0f * m_inverseMass) / (dimsSqr.y + dimsSqr.z);
	m_inverseInertia.y = (12.0f * m_inverseMass) / (dimsSqr.x + dimsSqr.z);
	m_inverseInertia.z = (12.0f * m_inverseMass) / (dimsSqr.x + dimsSqr.y);
}

void PhysicsObject::InitSphereInertia() {
	float radius	= m_transform->GetLocalScale().GetMaxElement();
	float i			= 2.5f * m_inverseMass / (radius*radius);

	m_inverseInertia = Vector3(i, i, i);
}

void PhysicsObject::UpdateInertiaTensor() {
	Quaternion q = m_transform->GetWorldOrientation();
	
	Matrix3 invOrientation	= q.Conjugate().ToMatrix3();
	Matrix3 orientation		= q.ToMatrix3();

	m_inverseInteriaTensor = orientation * Matrix3::Scale(m_inverseInertia) *invOrientation;
}