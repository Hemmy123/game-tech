#pragma once
#include "DynamicGObject.h"
#include "GameObject.h"

using namespace NCL::CSC8503;

class PlayerGObject :
	public DynamicGObject
{
public:
	PlayerGObject();
	virtual ~PlayerGObject();

	virtual void OnCollisionBegin(GameObject* otherObject) override;

	void setAlive(bool alive) { m_alive = alive; }
	bool isAlive() const { return m_alive; }

	void setReachedGoal(bool g) { m_reachedGoal = g; }
	bool getReachedGoal() const { return m_reachedGoal; }

	void setPosition(Vector3 worldPos) { m_transform.SetWorldPosition(worldPos); }

	void setInitialPosition(Vector3 init) { m_initialPosition = init; }
	Vector3 getInitialPosition() const { return m_initialPosition; }

private:
	Vector3 m_initialPosition;

	bool m_reachedGoal;
	bool m_alive;

};

