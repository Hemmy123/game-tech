#include "NavigationGrid.h"
#include "../../Common/Assets.h"

#include <fstream>
#include <queue>

using namespace NCL;
using namespace CSC8503;

const int LEFT_NODE		= 0;
const int RIGHT_NODE	= 1;
const int TOP_NODE		= 2;
const int BOTTOM_NODE	= 3;

const char WALL_NODE	= 'x';
const char FLOOR_NODE	= '.';

NavigationGrid::NavigationGrid(int nodeSize){
	m_nodeSize	= nodeSize;
	m_gridWidth	= 0;
	m_gridHeight= 0;
	m_allNodes	= nullptr;
}

NavigationGrid::NavigationGrid(const std::string&filename, int nodeSize) : NavigationGrid(nodeSize) {
	//std::ifstream infile(Assets::LEVELDIR + filename);

	m_fileReader.startReader(Assets::LEVELDIR + filename);

	std::string xSizeStr = m_fileReader.readLine();
	m_fileReader.stripCommas(xSizeStr);

	std::string ySizeStr = m_fileReader.readLine();
	m_fileReader.stripCommas(ySizeStr);

	m_gridWidth	 = std::stoi(xSizeStr);
	m_gridHeight = std::stoi(ySizeStr);

	m_closedList.reserve(m_gridWidth*m_gridHeight);

	//infile >> m_nodeSize;
	/*infile >> m_gridWidth;
	infile >> m_gridHeight;*/

	m_allNodes = new GridNode[m_gridWidth * m_gridHeight];

	for (int y = 0; y < m_gridHeight; ++y) {
		std::string line = m_fileReader.readLine();

		for (int x = 0; x < m_gridWidth; ++x) {
			GridNode&n = m_allNodes[(m_gridWidth * y) + x];
			m_fileReader.stripCommas(line);
			if (line.empty()) continue;

			char type = 0;
				type = line[x];
			n.type = type;
			n.visited = false;
			float posX = (float)(x * m_nodeSize);
			float posY = (float)(y * m_nodeSize);
			Vector3 pos(posX, 0, posY);

			n.position = Vector3(posX, 0, posY);
		}
	}
	
	//now to build the connectivity between the nodes
	for (int y = 0; y < m_gridHeight; ++y) {
		for (int x = 0; x < m_gridWidth; ++x) {
			GridNode&n = m_allNodes[(m_gridWidth * y) + x];		

			if (y > 0) { //get the above node
				n.connected[0] = &m_allNodes[(m_gridWidth * (y - 1)) + x];
			}
			if (y < m_gridHeight - 1) { //get the below node
				n.connected[1] = &m_allNodes[(m_gridWidth * (y + 1)) + x];
			}
			if (x > 0) { //get left node
				n.connected[2] = &m_allNodes[(m_gridWidth * (y)) + (x - 1)];
			}
			if (x < m_gridWidth - 1) { //get right node
				n.connected[3] = &m_allNodes[(m_gridWidth * (y)) + (x + 1)];
			}
			for (int i = 0; i < 4; ++i) {
				if ( n.connected[i] ) {
					if( n.connected[i] == NULL) {
						int i = 0;
					}

					if (n.connected[i]->type == '.' || n.connected[i]->type == 'M') {
						n.costs[i]		= 1;
					}
					else if (n.connected[i]->type == 'H') {
						n.connected[i] = nullptr; //actually a wall, disconnect!
					}
					else if (n.connected[i]->type == 'V') {
						n.connected[i] = nullptr; //actually a wall, disconnect!
					}
					else if (n.connected[i]->type == 'W') {
						n.connected[i] = nullptr; //actually a wall, disconnect!
					}
					
				}
			}
		}	
	}
}

NavigationGrid::~NavigationGrid()	{
	delete[] m_allNodes;
}

bool NavigationGrid::FindPath(const Vector3& from, const Vector3& to, NavigationPath& outPath) {

	ResetNodes();
	m_closedList.clear();
	// clear open list
	//m_openList = std::priority_queue<GridNode*, std::vector<GridNode*>, CompareGridNode>(CompareGridNode);

	// clear open list
	while(!m_openList.empty()) {
		m_openList.pop();
	}
	int fromX = (from.x / m_nodeSize);
	int fromZ = (from.z / m_nodeSize);

	int toX = (to.x / m_nodeSize);
	int toZ = (to.z / m_nodeSize);

/*
	int fromX = (from.x  );
	int fromZ = (from.z  );

	int toX = (to.x );
	int toZ = (to.z );*/

	// outside of map region
	if(fromX < 0 || fromX > m_gridWidth - 1 ||
		fromZ < 0 || fromZ > m_gridHeight -1) {
		return false;
	}


	// outside of map region
	if (toX < 0 || toX > m_gridWidth - 1 ||
		toZ < 0 || toZ > m_gridHeight - 1) {
		return false;
	}

	GridNode* startNode = &m_allNodes[(fromZ * m_gridWidth) + fromX];
	GridNode* endNode = &m_allNodes[(toZ * m_gridWidth) + toX];

	//std::priority_queue<GridNode*> openList;


	//std::vector<GridNode*> openList;
	//std::vector<GridNode*> closedList;

	//openList.emplace_back(startNode);
	m_openList.push(startNode);

	float startH = Heuristic(startNode, endNode);
	startNode->g = 0;

	startNode->f = startH + startNode->g;

	startNode->parent = nullptr;

	GridNode* currentBestNode = nullptr;

	while(!m_openList.empty()) {
		//currentBestNode = RemoveBestNode(openList);

		

		currentBestNode = m_openList.top(); // get the best node
		m_openList.pop();

		if(currentBestNode == endNode) {
			GridNode* node = endNode;
			while (node != nullptr) {
				outPath.PushWaypoint(node->position);
				node = node->parent;
			}
			return true;
		} else {

			// for the neighbouring nodes (does not include 
			// diagonal nodes
			for(int i = 0; i < 4; ++i) {
				GridNode* neighbour = currentBestNode->connected[i];

				// if there is no neighbour (a wall for example)
				if(!neighbour) {
					continue;
				}

				bool inClosed = NodeInList(neighbour, m_closedList);
				if(inClosed) {
					continue;
				}

				// Cost for neighbour!
				float h = Heuristic(neighbour, endNode);
				float g = currentBestNode->g + currentBestNode->costs[i];
				float f = h + g;

				//bool inOpen = NodeInList(neightbour, openList);
				bool inOpen = neighbour->visited;

				if(!inOpen) {
					//openList.emplace_back(neightbour);
					neighbour->visited = true;
					m_openList.push(neighbour);
				}

				if(!inOpen || f < neighbour->f) {
					neighbour->parent = currentBestNode;
					neighbour->f = f;
					neighbour->g = g;
				}
			}
			m_closedList.emplace_back(currentBestNode);
		}

	}

	return false; //open list emptied out with no path!
}

bool NavigationGrid::NodeInList(GridNode* n, std::vector<GridNode*>& list) const {
	
	auto i = std::find(list.begin(), list.end(), n);

	return i == list.end() ? false: true;
}

void NavigationGrid::ResetNodes() {
	for(int i = 0; i < m_gridHeight * m_gridWidth; ++i) {
		m_allNodes[i].visited = false;
	}

}


GridNode*  NavigationGrid::RemoveBestNode(std::vector<GridNode*>& list) const {
	auto bestI = list.begin();

	GridNode* bestNode = *list.begin();

	for(auto i = list.begin(); i != list.end();  ++i) {
		if( (*i)->f < bestNode->f) {
			bestNode	= (*i);
			bestI		= i;
		}
	}
	list.erase(bestI);
	return bestNode;


}

float NavigationGrid::Heuristic(GridNode* hNode, GridNode* endNode) const {

	return (hNode->position - endNode->position).Length();
}