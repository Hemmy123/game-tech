#pragma once
#include "NetworkBase.h"
#include <stdint.h>
#include <thread>
#include <atomic>
#include "GameObject.h"

namespace NCL {
	namespace CSC8503 {
		class GameObject;
		class GameClient : public NetworkBase {
		public:
			GameClient();
			~GameClient();

			bool Connect(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum);

			void SendPacket(GamePacket&  payload);
			void UpdateClient();

			bool getPositionUpdated() const { return m_positionUpdated; }
			
			std::vector<GameObjectState> getUpdatedStates() const { return m_updatedStates; };

			void clearStates() { m_updatedStates.clear(); }

		protected:

			// Is there a position update?
			bool m_positionUpdated;
			std::vector<GameObjectState> m_updatedStates;

			void ThreadedUpdate();

			ENetPeer*	m_netPeer;
			std::atomic<bool>	m_threadAlive;
			std::thread			m_updateThread;
		};
	}
}

