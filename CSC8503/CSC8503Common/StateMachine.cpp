#include "StateMachine.h"
#include "State.h"
#include "StateTransition.h"
#include <winerror.h>

using namespace NCL::CSC8503;

StateMachine::StateMachine()
{
	m_activeState = nullptr;
	m_lastTransition = nullptr;
}

StateMachine::~StateMachine()
{
}

void StateMachine::AddState(State* s) {
	m_allStates.emplace_back(s);

	if(m_activeState == nullptr){
		m_activeState = s;
	}

}

void StateMachine::AddTransition(StateTransition* t) {
	m_allTransitions.insert( {t->GetSourceState(), t} );
}

void StateMachine::Update() {

	if(m_activeState)
	{
		m_activeState->Update();

		// get the transition set starting from the this state node
		std::pair<TransitionIterator, TransitionIterator> range = m_allTransitions.equal_range(m_activeState);

		// iterate through the transitions
		for(auto& i = range.first; i != range.second; ++i) {
			if(i->second->CanTransition()) {
				State* newState = i->second->GetDestinationState();
				// change active state to the new state.
				m_activeState = newState;
				m_lastTransition = i->second;
			}
		}
	}
}