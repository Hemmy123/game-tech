#pragma once
#include "NavigationGrid.h"
#include "NavigationMap.h"
#include "NavigationPath.h"

#include "GameObject.h"


using namespace NCL::CSC8503;

/*
 * Class that helps move an object from A to B
 *
 */
class ObjectNavigator
{
public:
	ObjectNavigator();
	~ObjectNavigator();

	void update();

	void setPath(std::vector<Vector3> path);

	void setObject(GameObject* obj) { m_gameObject = obj; }
	GameObject* getObject() const { return m_gameObject; }

	void clearPath() { m_vectorPath.clear(); }
	void clearObject() { m_gameObject = nullptr; }
	void clearAll() { clearPath(); clearObject(); }
private:

	/// checks if the gameObject has reached the end position
	bool isAt(Vector3 a, Vector3 b);

	GameObject* m_gameObject;

	int m_vectorPathIndex;
	float speed = 100.0f;

	Vector3 m_currentDestination;
	std::vector<Vector3> m_vectorPath;


};

