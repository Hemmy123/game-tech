#include "PhysicsSystem.h"
#include "PhysicsObject.h"
#include "GameObject.h"
#include "CollisionDetection.h"
#include "../../Common/Quaternion.h"

#include "Constraint.h"

#include "Debug.h"

#include <functional>
using namespace NCL;
using namespace CSC8503;

/*

These two variables help define the relationship between positions
and the forces that are added to objects to change those positions

*/

//const float PhysicsSystem::UNIT_MULTIPLIER = 100.0f;
//const float PhysicsSystem::UNIT_RECIPROCAL = 1.0f / UNIT_MULTIPLIER;

const float PhysicsSystem::UNIT_MULTIPLIER = 1.0f;
const float PhysicsSystem::UNIT_RECIPROCAL = 1.0f;

PhysicsSystem::PhysicsSystem(GameWorld& g) : m_gameWorld(g)	{
	m_applyGravity	= true;
	m_useBroadPhase	= false;	
	m_dTOffset		= 0.0f;
	m_globalDamping	= 0.85f;
	m_restTolerance = 2;
	m_wakingTolerance = 0.5;
	
	//SetGravity(Vector3(0.0f, -9.8f, 0.0f));
	//SetGravity(Vector3(0.0f, -500.8f, 0.0f));
	SetGravity(Vector3(0.0f, -900.8f, 0.0f));
}

PhysicsSystem::~PhysicsSystem()	{
}

void PhysicsSystem::SetGravity(const Vector3& g) {
	m_gravity = g * UNIT_MULTIPLIER;
}

/*

If the 'game' is ever reset, the PhysicsSystem must be
'cleared' to remove any old collisions that might still
be hanging around in the collision list. If your engine
is expanded to allow objects to be removed from the world,
you'll need to iterate through this collisions list to remove
any collisions they are in.

*/
void PhysicsSystem::Clear() {
	m_allCollisions.clear();
}

/*

This is the core of the physics engine update

*/
void PhysicsSystem::Update(float dt) {

	const float iterationDt = 1.0f / 120.0f; //Ideally we'll have 120 physics updates a second 
	m_dTOffset += dt; //We accumulate time delta here - there might be remainders from previous frame!

	int iterationCount = (int)(m_dTOffset / iterationDt); //And split it up here

	float subDt = dt / (float)iterationCount;	//How many seconds per iteration do we get?

	IntegrateAccel(dt); //Update accelerations from external forces

	

	for (int i = 0; i < iterationCount; ++i) {
		if (m_useBroadPhase) {
			BroadPhase();
			NarrowPhase();
		}
		else {
			BasicCollisionDetection();
		}

		//This is our simple iterative solver - 
		//we just run things multiple times, slowly moving things forward
		//and then rechecking that the constraints have been met

		int constraintIterationCount = 10;
		float constraintDt = subDt / (float)constraintIterationCount;

		

		StartPosAccumulator();
		for (int i = 0; i < constraintIterationCount; ++i) {
			UpdateConstraints(constraintDt);
			IntegrateVelocity(constraintDt); //update positions from new velocity changes
		}
		EndPosAccumulator();

		m_dTOffset -= iterationDt;
	}
	ClearForces();	//Once we've finished with the forces, reset them to zero

	UpdateCollisionList(); //Remove any old collisions
}

void PhysicsSystem::Update(float dt, int iterations) {
	for (int i = 0; i < iterations; ++i) {
		IntegrateVelocityNetwork(dt); //update positions from new velocity changes
	}


}

/*
Later on we're going to need to keep track of collisions
across multiple frames, so we store them in a set.

The first time they are added, we tell the objects they are colliding.
The frame they are to be removed, we tell them they're no longer colliding.

From this simple mechanism, we we build up gameplay interactions inside the
OnCollisionBegin / OnCollisionEnd functions (removing health when hit by a 
rocket launcher, gaining a point when the player hits the gold coin, and so on).
*/
void PhysicsSystem::UpdateCollisionList() {
	for (std::set<CollisionDetection::CollisionInfo>::iterator i = m_allCollisions.begin(); i != m_allCollisions.end(); ) {
		if ((*i).framesLeft == m_numCollisionFrames) {
			i->a->OnCollisionBegin(i->b);
			i->b->OnCollisionBegin(i->a);
		}
		(*i).framesLeft = (*i).framesLeft - 1;
		if ((*i).framesLeft < 0) {
			i->a->OnCollisionEnd(i->b);
			i->b->OnCollisionEnd(i->a);
			i = m_allCollisions.erase(i);
		}
		else {
			++i;
		}
	}
}

void PhysicsSystem::UpdateObjectAABB() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last);

	for(auto i = first; i != last; ++i) {
		(*i)->UpdateBroadphaseAABB();
	}


}


/*

This is how we'll be doing collision detection in tutorial 4.
We step thorugh every pair of objects once (the inner for loop offset 
ensures this), and determine whether they collide, and if so, add them
to the collision set for later processing. The set will guarantee that
a particular pair will only be added once, so objects colliding for
multiple frames won't flood the set with duplicates.
*/
void PhysicsSystem::BasicCollisionDetection() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	m_gameWorld.GetObjectIterators(first, last);

	for(auto i = first; i != last; ++i) {
		
		if( (*i)->GetPhysicsObject() == nullptr) {
			continue;
		}

		for (auto j = i + 1; j != last; ++j) {
			if ((*j)->GetPhysicsObject() == nullptr) {
				continue;
			}
			CollisionDetection::CollisionInfo info;

			if(CollisionDetection::ObjectIntersection(*i, *j, info)) {
			
				ImpulseResolveCollision(*info.a, *info.b, info.point);			
				info.framesLeft = m_numCollisionFrames;
				m_allCollisions.insert(info);

				Vector3 oldVelA = (*i)->GetPhysicsObject()->GetLinearVelocity();
				Vector3 oldVelB = (*j)->GetPhysicsObject()->GetLinearVelocity();

				//if(oldVel != Vector3()) {
					/*float frictionA =  (*i)->GetPhysicsObject()->getFriction();
					float frictionB =  (*j)->GetPhysicsObject()->getFriction();

					if(frictionA >= 0.6f || frictionB >= 0.6f ) {
						float newFiction = 1 - frictionA * frictionB;
						Vector3 newVelocityA(oldVelA * newFiction);
						Vector3 newVelocityB(oldVelB * newFiction);

						(*i)->GetPhysicsObject()->SetLinearVelocity(newVelocityA);
						(*j)->GetPhysicsObject()->SetLinearVelocity(newVelocityB);
					}
*/
					

				//}

			}

		}
	}

}

void PhysicsSystem::ProjectResolveCollision(GameObject& a, GameObject& b, CollisionDetection::ContactPoint& p) const
{
	PhysicsObject* physA = a.GetPhysicsObject();
	PhysicsObject* physB = b.GetPhysicsObject();

	Transform& transformA = a.GetTransform();
	Transform& transformB = b.GetTransform();

	float totalMass = physA->GetInverseMass() + physB->GetInverseMass();
	if(totalMass == 0.0f)
	{
		return;
	}


	transformA.SetWorldPosition(transformA.GetWorldPosition() - (p.normal * p.penetration * (physA->GetInverseMass() / totalMass)));
	transformB.SetWorldPosition(transformB.GetWorldPosition() + (p.normal * p.penetration * (physB->GetInverseMass() / totalMass)));


	/*transformA.SetWorldPosition(transformA.GetLocalPosition() - (p.normal * p.penetration * (physA->GetInverseMass() / totalMass)));
	transformB.SetWorldPosition(transformB.GetLocalPosition() + (p.normal * p.penetration * (physB->GetInverseMass() / totalMass)));
*/

}

/*

In tutorial 5, we start determining the correct response to a collision,
so that objects separate back out. 

*/
void PhysicsSystem::ImpulseResolveCollision(GameObject& a, GameObject& b, CollisionDetection::ContactPoint& p) const {
	
	// Separating them using projection
	ProjectResolveCollision(a, b, p);

	PhysicsObject* physA = a.GetPhysicsObject();
	PhysicsObject* physB = b.GetPhysicsObject();


	Transform& transformA = a.GetTransform();
	Transform& transformB = b.GetTransform();

	float totalMass = physA->GetInverseMass() + physB->GetInverseMass();

	Vector3 relativeA = p.position - transformA.GetWorldPosition();
	Vector3 relativeB = p.position - transformB.GetWorldPosition();


	//Vector3 relativeA = p.position - transformA.GetLocalPosition();
	//Vector3 relativeB = p.position - transformB.GetLocalPosition();


	Vector3 angVelocityA = Vector3::Cross(physA->GetAngularVelocity(), relativeA);
	Vector3 angVelocityB = Vector3::Cross(physB->GetAngularVelocity(), relativeB);

	Vector3 fullVelocityA = physA->GetLinearVelocity() + angVelocityA;
	Vector3 fullVelocityB = physB->GetLinearVelocity() + angVelocityB;

	Vector3 contactVelocity = fullVelocityB - fullVelocityA;

	// if objects are going the right way don't apply impluse
	if(Vector3::Dot(contactVelocity, p.normal) > 0)
	{
		return;
	}


	float impluseForce = Vector3::Dot(contactVelocity, p.normal);

	Vector3 inertiaA = Vector3::Cross(physA->GetInertiaTensor() * Vector3::Cross(relativeA, p.normal), relativeA);
	Vector3 inertiaB = Vector3::Cross(physB->GetInertiaTensor() * Vector3::Cross(relativeB, p.normal), relativeB);

	float angularEffect = Vector3::Dot(inertiaA + inertiaB, p.normal);

	float cRestitution = 0.66;  // 'e'

	float  j = (-(1.0f + cRestitution) * impluseForce) / (totalMass + angularEffect);
	//float  j = (-(1.0f + cRestitution) * impluseForce) / (totalMass);

	Vector3 fullImpluse = p.normal * j;


	//testing
	auto currentVelA = physA->GetLinearVelocity();
	auto currentVelB = physB->GetLinearVelocity();

	physA->ApplyLinearImpulse(-fullImpluse);
	physB->ApplyLinearImpulse(fullImpluse);

	Vector3 angularImpluseA = Vector3::Cross(relativeA, -fullImpluse);
	Vector3 angularImpluseB = Vector3::Cross(relativeB, fullImpluse);

	physA->ApplyAngularImpulse(angularImpluseA);
	physB->ApplyAngularImpulse(angularImpluseB);
	
}

void PhysicsSystem::StartPosAccumulator() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last);
	for (auto i = first; i != last; ++i) {
		PhysicsObject* object = (*i)->GetPhysicsObject();
		object->setCumulativePos(Vector3());
	}
}

void PhysicsSystem::EndPosAccumulator() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last);
	for (auto i = first; i != last; ++i) {
		PhysicsObject* object = (*i)->GetPhysicsObject();

		if(object->getCumulativePos().Length() < m_restTolerance) {
			GameObjectType type = (*i)->GetType();
			//object->SetAtRest(true);
		} 
	}

}

/*

Later, we replace the BasicCollisionDetection method with a broadphase
and a narrowphase collision detection method. In the broad phase, we
split the world up using an acceleration structure, so that we can only
compare the collisions that we absolutely need to. 

*/
void PhysicsSystem::BroadPhase() {
	m_broadphaseCollisions.clear();
	QuadTree<GameObject*> tree(Vector2(1024, 1024), 7, 6);

	tree.DebugDraw();

	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	m_gameWorld.GetObjectIterators(first, last);
	for(auto i = first; i != last; ++i) {
		Vector3 halfSize;
		if( !(*i)->GetBroadphaseAABB(halfSize)) {
			continue;
		}
		Vector3 pos = (*i)->GetConstTransform().GetWorldPosition();
		tree.Insert(*i, pos, halfSize);

	}


	tree.OperateOnContents([&](std::list<QuadTreeEntry<GameObject*>>& data ) {
		CollisionDetection::CollisionInfo info;

		for(auto i = data.begin(); i != data.end(); ++i) {
			for(auto j = std::next(i); j != data.end(); ++j) {
				info.a = min((*i).m_object, (*j).m_object);
				info.b = max((*i).m_object, (*j).m_object);
				m_broadphaseCollisions.insert(info);

			}
		}


	});



}

/*

The broadphase will now only give us likely collisions, so we can now go through them,
and work out if they are truly colliding, and if so, add them into the main collision list
*/
void PhysicsSystem::NarrowPhase() {
	for(std::set<CollisionDetection::CollisionInfo>::iterator
		i = m_broadphaseCollisions.begin();
		i != m_broadphaseCollisions.end(); ++i) {
		CollisionDetection::CollisionInfo info = *i;

		if(CollisionDetection::ObjectIntersection(info.a, info.b, info)) {
			info.framesLeft = m_numCollisionFrames;
			ImpulseResolveCollision(*info.a, *info.b, info.point);
			m_allCollisions.insert(info);
		}

	}


}

/*
Integration of acceleration and velocity is split up, so that we can
move objects multiple times during the course of a PhysicsUpdate,
without worrying about repeated forces accumulating etc. 

This function will update both linear and angular acceleration,
based on any forces that have been accumulated in the objects during
the course of the previous game frame.
*/
void PhysicsSystem::IntegrateAccel(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last);

	for(auto i = first; i != last; ++i){
		PhysicsObject *object = (*i)->GetPhysicsObject();

		if(object == nullptr)
		{
			continue; // This gameobject doesn't have a physics object, so don't do anything
		}

		// ----- Linear Acceleration ----- //
		float inverseMass	= object->GetInverseMass();
		Vector3 linearVel	= object->GetLinearVelocity();
		Vector3 force		= object->GetForce();
		Vector3 accel		= force * inverseMass;

		const CollisionVolume* vol = object->GetVolume();

		if(m_applyGravity && (inverseMass > 0) ) {
			accel += m_gravity;
		}

		if (force.Length() > m_wakingTolerance) {
			object->SetAtRest(false);
		}


		linearVel += accel * dt; // integrate accel

		if(!object->isAtRest()) {
			object->SetLinearVelocity(linearVel);

		}

		Vector3 torque = object->GetTorque();
		Vector3 angVel = object->GetAngularVelocity();

		// update the matrix that allows us to get rotations 
		// for our object type
		object->UpdateInertiaTensor();

		// like f*(1/m), but using inertia tensor instead
		Vector3 angAccel = object->GetInertiaTensor() * torque;

		angVel += angAccel * dt;	// Integrate angular acceleration
		if (!object->isAtRest()) {
			object->SetAngularVelocity(angVel);
		}

		
		object->updateRest(dt);

	}
}
/*
This function integrates linear and angular velocity into
position and orientation. It may be called multiple times
throughout a physics update, to slowly move the objects through
the world, looking for collisions.
*/
void PhysicsSystem::IntegrateVelocity(float dt) {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last); 
	float dampingFactor = 1.0f - m_globalDamping;
	float frameDamping = powf(dampingFactor, dt);

	for(auto i = first; i != last; ++i)
	{
		PhysicsObject* object = (*i)->GetPhysicsObject();
		if(object == nullptr)
		{
			continue;
		}
		Transform& transform = (*i)->GetTransform();

		if(!object->isAtRest()) {
			//continue;
			// ----- Linear Velocity ----- //


			Vector3 position = transform.GetLocalPosition();
			Vector3 linearVel = object->GetLinearVelocity();
			Vector3 changeInPos = linearVel * dt;
			position += changeInPos;

			object->accumulatePos(changeInPos);


			transform.SetLocalPosition(position);
			transform.SetWorldPosition(position);
			//linear damping
			linearVel = linearVel * frameDamping;
			object->SetLinearVelocity(linearVel);
		} 
		


		// ----- Angular Velocity ----- //

		// If it's not an AABB
		if(object->GetVolume()->type != VolumeType::AABB)
		{
				Quaternion orientation = transform.GetLocalOrientation();
				Vector3 angVel = object->GetAngularVelocity();

				orientation = orientation +
					(Quaternion(angVel * dt * 0.5f, 0.0f) *orientation); // Integration step!

				orientation.Normalise();

				transform.SetLocalOrientation(orientation);

				angVel = angVel * frameDamping;
				object->SetAngularVelocity(angVel);
						
		} else {

			// Angular velocity because it's an AABB 
			object->SetAngularVelocity(Vector3());

		}
		

	}
}

void PhysicsSystem::IntegrateVelocityNetwork(float dt) {

	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;

	m_gameWorld.GetObjectIterators(first, last);
	float dampingFactor = 1.0f - 0.99;

	for (auto i = first; i != last; ++i)
	{
		PhysicsObject* object = (*i)->GetPhysicsObject();
		if (object == nullptr)
		{
			continue;
		}
		Transform& transform = (*i)->GetTransform();

		if (!object->isAtRest()) {
			//continue;
			// ----- Linear Velocity ----- //


			Vector3 position = transform.GetLocalPosition();
			Vector3 linearVel = object->GetLinearVelocity();
			Vector3 changeInPos = linearVel * dt;
			position += changeInPos;

			object->accumulatePos(changeInPos);


			transform.SetLocalPosition(position);
			transform.SetWorldPosition(position);
			//linear damping
			linearVel = linearVel * dampingFactor;
			object->SetLinearVelocity(linearVel);
		}
	}
}

/*
Once we're finished with a physics update, we have to
clear out any accumulated forces, ready to receive new
ones in the next 'game' frame.
*/
void PhysicsSystem::ClearForces() {
	std::vector<GameObject*>::const_iterator first;
	std::vector<GameObject*>::const_iterator last;
	m_gameWorld.GetObjectIterators(first, last);

	for (auto i = first; i != last; ++i) {
		//Clear our object's forces for the next frame
		(*i)->GetPhysicsObject()->ClearForces();
	}
}


/*

As part of the final physics tutorials, we add in the ability
to constrain objects based on some extra calculation, allowing
us to model springs and ropes etc. 

*/
void PhysicsSystem::UpdateConstraints(float dt) {
	std::vector<Constraint*>::const_iterator first;
	std::vector<Constraint*>::const_iterator last;
	m_gameWorld.GetConstraintIterators(first, last);

	for (auto i = first; i != last; ++i) {
		(*i)->UpdateConstraint(dt);
	}
}