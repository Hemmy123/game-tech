#pragma once
#include <thread>
#include <atomic>

#include "NetworkBase.h"
#include "PlayerGObject.h"
#include "../../Common/GameTimer.h"
#include "../GameTech/GameStateManager.h"

namespace NCL {
	namespace CSC8503 {
		class GameWorld;
		class GameServer : public NetworkBase {
		public:
			GameServer(int onPort, int maxClients);
			~GameServer();

			virtual void UpdateServer();
			void updateGame(float dt);


			bool Initialise();
			void Shutdown();

			void ThreadedUpdate();

			bool SendGlobalMessage(int msgID);
			bool SendGlobalMessage(GamePacket& packet);

			void BroadcastSnapshot(bool deltaFrame);
			void UpdateMinimumState();


			void updateHighscore(int playerID, int score );


			// tells everyone to load a specfic level
			void loadClientLevels(int level);

		protected:

			GameTimer m_timer;

			std::map<int, ENetPeer*> m_clientMap;

			bool m_levelLoaded;


			std::vector<RayCastData> m_raycastData;

			// ----- Pack processing ----- //

			bool ProcessPacket(GamePacket* packet, int peerID) override;

			void SendPacketToPeer(int client, GamePacket& packet) const;



			// ----- Game Logic stuff ----- //

			// player represented by player number
			std::map<int, int> m_highScores;
			std::map<int, string> m_playerNames;


			GameStateManager* m_serverGame;

			void resetHighScores();

			void printHighscores();
			// ------------------------------ //
			int			m_port;
			int			m_clientMax;
			int			m_clientCount;


			void initReceivers();


			std::atomic<bool> m_threadAlive;
		

			std::thread m_updateThread;

			int m_incomingDataRate;
			int m_outgoingDataRate;

			std::map<int, int> m_stateIDs;
		};
	}
}
