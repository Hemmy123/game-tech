#pragma once
#include "../../Common/Vector3.h"
#include "../../Common/Matrix3.h"

using namespace NCL::Maths;

namespace NCL {
	class CollisionVolume;
	
	namespace CSC8503 {
		class Transform;

		class PhysicsObject	{
		public:
			PhysicsObject(Transform* parentTransform, const CollisionVolume* parentVolume);
			~PhysicsObject();

			void updateRest(float dt);

			Vector3 GetLinearVelocity() const {
				return m_linearVelocity;
			}

			Vector3 GetAngularVelocity() const {
				return m_angularVelocity;
			}

			Vector3 GetTorque() const {
				return m_torque;
			}

			Vector3 GetForce() const {
				return m_force;
			}

			void SetInverseMass(float invMass) {
				m_inverseMass = invMass;
			}

			float GetInverseMass() const {
				return m_inverseMass;
			}

			void ApplyAngularImpulse(const Vector3& force);
			void ApplyLinearImpulse(const Vector3& force);
			
			void AddForce(const Vector3& force);

			void AddForceAtPosition(const Vector3& force, const Vector3& position);

			void AddTorque(const Vector3& torque);


			void ClearForces();

			void SetLinearVelocity(const Vector3& v) {
				m_linearVelocity = v;
			}

			void SetAngularVelocity(const Vector3& v) {
				m_angularVelocity = v;
			}

			void InitCubeInertia();
			void InitSphereInertia();

			void UpdateInertiaTensor();

			Matrix3 GetInertiaTensor() const {
				return m_inverseInteriaTensor;
			}

			const CollisionVolume* GetVolume() const { return m_volume; }

			void SetAtRest(bool r) { m_atRest = r; }
			bool isAtRest() const { return m_atRest; }

			void setFriction(float f) { m_friction = f; }
			float getFriction() const { return m_friction; }

			void accumulatePos(Vector3 a) { m_cumulativePos += a; }
			void setCumulativePos(Vector3 pos)		{ m_cumulativePos = pos; }
			Vector3 getCumulativePos() const		{ return m_cumulativePos; }

		protected:
			bool m_atRest;

			float m_restTime;

			const CollisionVolume* m_volume;
			Transform*		m_transform;

			// The change in position over a few frames.
			Vector3 m_cumulativePos;
			Vector3 m_cumulativeRot;

			float m_inverseMass;
			float m_elasticity;
			float m_friction;

			//linear stuff
			Vector3 m_linearVelocity;
			Vector3 m_force;

			//angular stuff
			Vector3 m_angularVelocity;
			Vector3 m_torque;
			Vector3 m_inverseInertia;
			Matrix3 m_inverseInteriaTensor;
		};
	}
}

