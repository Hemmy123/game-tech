#pragma once
#include "../CSC8503Common/GameWorld.h"
#include <set>

namespace NCL {
	namespace CSC8503 {
		class PhysicsSystem	{
		public:
			PhysicsSystem(GameWorld& g);
			~PhysicsSystem();

			void Clear();

			void Update(float dt);

			void Update(float dt, int iterations);

			void UseGravity(bool state) {
				m_applyGravity = state;
			}

			void SetGlobalDamping(float d) {
				m_globalDamping = d;
			}

			void SetGravity(const Vector3& g);

			static const float UNIT_MULTIPLIER;
			static const float UNIT_RECIPROCAL;

		protected:
			void BasicCollisionDetection();
			void BroadPhase();
			void NarrowPhase();

			void ClearForces();

			void IntegrateAccel(float dt);
			void IntegrateVelocity(float dt);
			void IntegrateVelocityNetwork(float dt);

			void UpdateConstraints(float dt);

			void UpdateCollisionList();

			void UpdateObjectAABB();

			void ProjectResolveCollision(GameObject& a, GameObject&b, CollisionDetection::ContactPoint& p) const;
			void ImpulseResolveCollision(GameObject& a , GameObject&b, CollisionDetection::ContactPoint& p) const;

			GameWorld& m_gameWorld;

			bool	m_applyGravity;
			Vector3 m_gravity;
			float	m_dTOffset;
			float	m_globalDamping;

			// the minimum distance an object has to move in
			// a few frames to be counted as resting
			float m_restTolerance;

			// the minimum acceleration (force) applied to an 
			// object before it can wake up
			float m_wakingTolerance;

			std::set<CollisionDetection::CollisionInfo> m_allCollisions;
			std::set<CollisionDetection::CollisionInfo> m_broadphaseCollisions;
			bool m_useBroadPhase		= true;
			int m_numCollisionFrames	= 5;


			void StartPosAccumulator();
			void EndPosAccumulator();
		};
	}
}

