#pragma once
#include "Transform.h"
#include "CollisionVolume.h"

#include "PhysicsObject.h"
#include "RenderObject.h"
#include "NetworkObject.h"

#include <vector>

using std::vector;

namespace NCL {
	namespace CSC8503 {
		class NetworkObject;

		enum class GameObjectType{
			Player,
			Enemy,
			Goal,
			Windmill,
			Floor,
			Mace,
		
			StaticBox,	// For walls and floors, could be ABB or OBB depending on rotation
			
			DynamicBox,	// For boxes that need to be affected by gravity
			AABBDynamicBoxes	// For boxes that need to be affected by gravity

		};

		struct GameObjectState {
			GameObjectState(
				int id,
				Vector3 position,
				Vector3 linearVelocity,
				Quaternion orientation
			) :
				m_id(id),
				m_position(position),
				m_linearVelocity(linearVelocity),
				m_orientation(orientation) {};

			GameObjectState() {
				m_id = 0;
				m_position = Vector3();
				m_linearVelocity = Vector3();
				m_orientation = Quaternion();
			}


			int m_id;
			Vector3 m_position;
			Vector3 m_linearVelocity;
			Quaternion m_orientation;
		};


		class GameObject	{
		public:
			GameObject(string name = "");
			virtual ~GameObject();

			void SetBoundingVolume(CollisionVolume* vol) {
				m_boundingVolume = vol;
			}

			const CollisionVolume* GetBoundingVolume() const {
				return m_boundingVolume;
			}

			bool IsActive() const {
				return m_isActive;
			}

			void SetActive(bool active) {
				m_isActive = active;
			}

			const Transform& GetConstTransform() const {
				return m_transform;
			}

			Transform& GetTransform() {
				return m_transform;
			}

			RenderObject* GetRenderObject() const {
				return m_renderObject;
			}

			PhysicsObject* GetPhysicsObject() const {
				return m_physicsObject;
			}

			NetworkObject* GetNetworkObject() const {
				return m_networkObject;
			}

			void SetRenderObject(RenderObject* newObject) {
				m_renderObject = newObject;
			}

			void SetPhysicsObject(PhysicsObject* newObject) {
				m_physicsObject = newObject;
			}

			const string& GetName() const {
				return m_name;
			}

			virtual void OnCollisionBegin(GameObject* otherObject) {
				//std::cout << "OnCollisionBegin event occured!\n";
			}

			virtual void OnCollisionEnd(GameObject* otherObject) {
				//std::cout << "OnCollisionEnd event occured!\n";
			}

			bool InsideAABB(const Vector3& pos, const Vector3& halfSize);


			GameObjectType GetType() const { return m_objectType; }
			void SetType(GameObjectType type) { m_objectType = type; }

			// What happens when two specific objects collide 
			virtual void  CollidingWith(GameObject* a, GameObject* b);

			bool GetBroadphaseAABB(Vector3& outsize) const;
			void UpdateBroadphaseAABB();



			void setId(int i) { m_id = i; }
			int getId()const { return m_id; }
		protected:
			int m_id;

			Vector3 m_broadphaseAABB;

			Transform			m_transform;
			GameObjectType		m_objectType;
			CollisionVolume*	m_boundingVolume;
			PhysicsObject*		m_physicsObject;
			RenderObject*		m_renderObject;
			NetworkObject*		m_networkObject;

			bool	m_isActive;
			string	m_name;
		};
	}
}

