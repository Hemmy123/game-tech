#pragma once
#include "DynamicGObject.h"
#include "StateMachine.h"

enum class Direction {
	Up,
	Down,
	Left,
	Right
};

using namespace NCL::CSC8503;

class PatrollingGObject : public DynamicGObject
{
public:
	PatrollingGObject(Vector3 posA, Vector3 posB);
	PatrollingGObject():m_posA(Vector3()), m_posB(Vector3()) {
		m_fsm = new StateMachine();
		m_speed = 0.5;
	};
	virtual ~PatrollingGObject();


	void update() {
		updateDistance();
		m_fsm->Update();
	}
	
	void initStates();

	void setPosA(Vector3 posA) { m_posA = posA; };
	void setPosB(Vector3 posB) { m_posB = posB; };
	void setSpeed(float s) { m_speed = s; }
	
private:
	void moveToA();
	void moveToB();

	
	void updateDistance();


	StateMachine* m_fsm;

	float	m_distanceToA;
	float	m_distanceToB;

	Vector3 m_posA;
	Vector3 m_posB;
	float m_speed;


};

