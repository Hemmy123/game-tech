#include "GameClient.h"
#include <iostream>
#include <string>

using namespace NCL;
using namespace CSC8503;

GameClient::GameClient()	{
	m_netPeer = nullptr;
	m_netHandle = enet_host_create(nullptr, 1, 1, 0, 0);
}

GameClient::~GameClient()	{
	m_threadAlive = false;
	m_updateThread.join();
	enet_host_destroy(m_netHandle);
}

bool GameClient::Connect(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum) {
	ENetAddress address;
	address.port = portNum;
	address.host = (d << 24) | (c << 16) | (b << 8) | (a);

	// tries to connect to server.
	m_netPeer = enet_host_connect(m_netHandle, &address, 2, 0);

	return m_netPeer != nullptr;
}

void GameClient::UpdateClient() {
	if(m_netHandle == nullptr) {
		return;
	}
	m_positionUpdated = false;

	// Handle all incoming packets
	ENetEvent event;
	while(enet_host_service(m_netHandle, &event, 0) > 0) {

		if(event.type == ENET_EVENT_TYPE_CONNECT) {
			std::cout << "Client: Connected to server!" << std::endl;
		}
		else if(event.type == ENET_EVENT_TYPE_RECEIVE) {
			std::cout << "Client: Packet Received..." << std::endl;
			GamePacket* packet = (GamePacket*)event.packet->data;
			ProcessPacket(packet);

			if(packet->type == Full_State) {
				GameObjectStatePacket* statepacket = (GameObjectStatePacket*)packet;
				GameObjectState temp(
					statepacket->m_id,
					statepacket->m_position,
					statepacket->m_linearVelocity,
					statepacket->m_orientation);

				m_updatedStates.push_back(temp);
				m_positionUpdated = true;
			}

		}

		enet_packet_destroy(event.packet);
	}



}

void GameClient::SendPacket(GamePacket&  payload) {

	// Creates eNet packet
	ENetPacket* dataPacket = enet_packet_create(&payload, payload.GetTotalSize(), 0);

	enet_peer_send(m_netPeer, 0, dataPacket);

}

void GameClient::ThreadedUpdate() {
	while (m_threadAlive) {
		UpdateClient();
		std::this_thread::yield();
	}
}
