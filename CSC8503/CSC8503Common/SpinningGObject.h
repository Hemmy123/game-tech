#pragma once
#include "DynamicGObject.h"

enum class RotationOrientation {
	Horizontal,
	Vertical
};

class SpinningGObject : public DynamicGObject {
public:
	SpinningGObject(float speed, RotationOrientation orientation): 
	m_speed(speed), 
	m_orientation(orientation){};

	~SpinningGObject(){};

	float getSpeed() { return m_speed; }
	RotationOrientation getOrientation() { return m_orientation; }



private:
	float m_speed;
	RotationOrientation m_orientation;


};

