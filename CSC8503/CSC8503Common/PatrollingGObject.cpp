#include "PatrollingGObject.h"
#include "State.h"
#include "StateTransition.h"


PatrollingGObject::PatrollingGObject(Vector3 posA, Vector3 posB):
m_posA(posA),
m_posB(posB)
{
	m_fsm = new StateMachine();
	m_speed = 0.5;
}


PatrollingGObject::~PatrollingGObject()
{
	delete m_fsm;
}

void PatrollingGObject::moveToA() {
	Vector3 direction = (m_posA - m_transform.GetWorldPosition()).Normalised();
	m_physicsObject->SetLinearVelocity(direction * m_speed);
}

void PatrollingGObject::moveToB() {
	Vector3 direction = (m_posB - m_transform.GetWorldPosition()).Normalised();
	m_physicsObject->SetLinearVelocity(direction * m_speed);
}


void PatrollingGObject::initStates() {


	StateFunc moveTowardsA = [](void* data)
	{
		PatrollingGObject* maceObj = (PatrollingGObject*)data;
		maceObj->moveToA();
	};

	StateFunc moveTowardsB = [](void* data)
	{
		PatrollingGObject* maceObj = (PatrollingGObject*)data;
		maceObj->moveToB();
	};


	GenericState* movingToA	= new GenericState(moveTowardsA, this);
	GenericState* movingToB = new GenericState(moveTowardsB, this);
	movingToA->SetName("Moving To A");
	movingToB->SetName("Moving To B");

	m_fsm->AddState(movingToA);
	m_fsm->AddState(movingToB);

	GenericTransition<float&, float>* BtoATran =
		new GenericTransition<float&, float>(
			GenericTransition<float&, float>::LessThanTransition, m_distanceToB, 1, movingToB, movingToA
			);
	BtoATran->setName("moveTowardsA");


	GenericTransition<float&, float>* AtoBTran =
		new GenericTransition<float&, float>(
			GenericTransition<float&, float>::LessThanTransition, m_distanceToA, 1.0f, movingToA, movingToB
			);
	AtoBTran->setName("moveTowardsB");

	m_fsm->AddTransition(BtoATran);
	m_fsm->AddTransition(AtoBTran);
}



void PatrollingGObject::updateDistance() {
	m_distanceToA = (m_posA - m_transform.GetWorldPosition()).Length();
	m_distanceToB = (m_posB - m_transform.GetWorldPosition()).Length();

}
