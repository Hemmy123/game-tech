#pragma once
#include <string>
#include <functional>

namespace NCL {
	namespace CSC8503 {
		class State		{
		public:
			State();
			virtual ~State();
			virtual void Update() = 0; //Pure virtual base class
		};

		// a pointer to a lambda function 'StateFunc' which takes in a void pointer and returns void
		// Note: does not include capture list!
		typedef void(*StateFunc)(void*);

		class GenericState : public State		{
		public:
			GenericState(StateFunc someFunc, void* someData) {
				func		= someFunc;
				funcData	= someData;
				name		= "";
			}
			virtual void Update() {
				if (funcData != nullptr) {
					func(funcData);
				}
			}

			void SetName(std::string n) { name = n; }
		protected:
			std::string  name;
			StateFunc func;
			void* funcData;
		};




		class GameState : public State {
		public:
			GameState(std::function<void(void*)> someFunc, void* someData) {
				func = someFunc;
				funcData = someData;
				name = "";
			}
			virtual void Update() {
				if (funcData != nullptr) {
					func(funcData);
				}
			}

			void SetName(std::string n) { name = n; }
			void SetLevel(int l) { level = l; }
		protected:
			std::string  name;
			std::function<void(void*)> func;
			int level;
			void* funcData;
		};







	}
}
