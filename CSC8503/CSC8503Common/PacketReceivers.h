#pragma once
#include <string>
#include <iostream>
#include "NetworkBase.h"
#include "../GameTech/NetworkedGame.h"
using std::string;

class BasicReceiver : public PacketReceiver {
public:
	BasicReceiver(string name) : m_name(name) {};
	~BasicReceiver(){};

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if (type == String_Message) {
			StringPacket *realPacket = (StringPacket*)payload;
			string msg = realPacket->GetStringFromData();
			std::cout << m_name << " received message: " << msg << std::endl;
		}
	}

protected:
	string m_name;
};


class HighScoreReceiver: public PacketReceiver {
public:
	HighScoreReceiver(string name, std::map<int, int>* scoreMap) : m_name(name) {
		m_score = 0;
		m_scoreMap = scoreMap;
	};
	~HighScoreReceiver() {};

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if (type == HighScore) {
			SingleHighScorePacket *realPacket = (SingleHighScorePacket*)payload;
			m_score			= realPacket->m_score;
			m_sender		= realPacket->m_id;
			m_scoreMap->at(m_sender) = m_score;
			m_sender		= source;
		}

		// updates the score in m_scoreMap with the one
		// that was sent via the server.
		if(type == HighScore_Boardcast) {
			HighScoreMapPacket *realPacket = (HighScoreMapPacket*)payload;
			
			int size = realPacket->m_elems;

			for (int i = 0; i < size; i = i + 2) {
				int id = realPacket->m_highScores[i];
				int data = realPacket->m_highScores[i + 1];

				m_scoreMap->at(id) = data;
			}
		}
	}

private:
	std::map<int, int>* m_scoreMap;
	int m_score;
	int	m_sender;
	string m_name;
};

class StateReceiver: public PacketReceiver {
public:

	StateReceiver(string name, GameObjectState& state):
	m_name(name),
	m_id(state.m_id),
	m_posRef(state.m_position),
	m_velocity(state.m_linearVelocity),
	m_oriRef(state.m_orientation),
	m_stateRef(state){
			
	}


	~StateReceiver() {};

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if(type == Full_State) {
			GameObjectStatePacket*  realPacket = (GameObjectStatePacket*)payload;
			m_id		= realPacket->m_id;
			m_posRef	= realPacket->m_position;
			m_velocity	= realPacket->m_linearVelocity;
			m_oriRef	= realPacket->m_orientation;
			m_stateRef = GameObjectState(m_id, m_posRef, m_velocity, m_oriRef);
		}
	}

private:
	int&		m_id;
	Vector3&	m_posRef;
	Vector3&	m_velocity;
	Quaternion& m_oriRef;
	GameObjectState& m_stateRef;


	string m_name;
};


/// receives an int to represent which level to load
/// and the bool to filp to indicate that a new level
/// has to be loaded
class LevelReceiver: public PacketReceiver {
public:

	LevelReceiver(string name, int& levelToChange, bool& boolToChange): m_name(name) {
		m_levelToChange = &levelToChange;
		m_boolToChange	= &boolToChange;
	}

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if(type == New_Level) {
			LoadLevelPacket* realPacket = (LoadLevelPacket*)payload;
			*m_levelToChange = realPacket->m_level;
			*m_boolToChange = true;
		}
		
	}


private:

	int* m_levelToChange;
	bool* m_boolToChange;
	int	m_sender;
	string m_name;

};

class IntReceiver: public PacketReceiver {
public:

	IntReceiver(string name, int& toChange) : m_name(name) {
		m_intReceived = 0;
		m_toChange = &toChange;
	};
	~IntReceiver() {};

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if (type == Int_Message) {
			IntPacket *realPacket = (IntPacket*)payload;
			m_intReceived = realPacket->m_int;
			*m_toChange = m_intReceived;
			m_sender = source;
		}

		if (type == Player_Connected) {
			NewPlayerPacket *realPacket = (NewPlayerPacket*)payload;
			m_intReceived = realPacket->playerID;
			*m_toChange = m_intReceived;
			m_sender = source;
		}

		if (type == New_Level) {
			LoadLevelPacket *realPacket = (LoadLevelPacket*)payload;
			m_intReceived = realPacket->m_level;
			*m_toChange = m_intReceived;

		}

	}

	int GetInt() { return m_intReceived; }

private:
	int* m_toChange;
	int m_intReceived;
	int	m_sender;
	string m_name;
};

class RayCastReceiver:public PacketReceiver {
public:

	RayCastReceiver(string name,std::vector<RayCastData>* rayData) :
	m_name(name),
	m_rayData(rayData){


	};

	~RayCastReceiver() {};

	void ReceivePacket(int type, GamePacket* payload, int source) override {
		if (type == RayCast_Force) {
			RayCastDataPacket *realPacket = (RayCastDataPacket*)payload;

			RayCastData data(
				realPacket->m_id,
				realPacket->m_direction, 
				realPacket->m_collidedAt, 
				realPacket->m_forceMagnitude);
			m_rayData->push_back(data);

		}
	}


private:
	string m_name;
	std::vector<RayCastData>* m_rayData;

};
