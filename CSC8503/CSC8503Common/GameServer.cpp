#include "GameServer.h"
#include "GameWorld.h"
#include <iostream>
#include "PacketReceivers.h"

using namespace NCL;
using namespace CSC8503;

GameServer::GameServer(int onPort, int maxClients)	{
	m_port		= onPort;
	m_clientMax	= maxClients;
	m_clientCount = 0;
	m_netHandle	= nullptr;
	m_threadAlive = false;



	Initialise();
	initReceivers();
	m_serverGame = new GameStateManager;
	m_serverGame->setMultiplayer(true);
	m_serverGame->setIsServer(true);
}

GameServer::~GameServer()	{
	Shutdown();
}

void GameServer::Shutdown() {
	SendGlobalMessage(BasicNetworkMessages::Shutdown);

	m_threadAlive = false;
	m_updateThread.join();

	enet_host_destroy(m_netHandle);
	m_netHandle = nullptr;



}

bool GameServer::Initialise() {
	ENetAddress address;
	address.host = ENET_HOST_ANY;
	address.port = m_port;

	// Creating the server
	m_netHandle = enet_host_create(&address, m_clientMax, 1, 0, 0);

	if(!m_netHandle) {
		std::cout << __FUNCTION__ << "Failed to create network handle!" << std::endl;
		return false;
	}

	return true;
}

bool GameServer::SendGlobalMessage(int msgID) {
	GamePacket packet;
	packet.type = msgID;
	return SendGlobalMessage(packet);

	return true;
}

bool GameServer::SendGlobalMessage(GamePacket& packet) {
	// Creates the eNet packet
	ENetPacket*  dataPacket = enet_packet_create(&packet, packet.GetTotalSize(), 0);

	// Broadcasts that to all clients
	enet_host_broadcast(m_netHandle, 0, dataPacket);

	return true;
}

void GameServer::UpdateServer() {

	if (!m_netHandle) { return; }
	ENetEvent event;

	// pops off an event from eNet's internal queue
	while (enet_host_service(m_netHandle, &event, 0) > 0) {
		int type		= event.type;
		ENetPeer*  p	= event.peer;
		int peer		= p->incomingPeerID;

		// Connecting new client;
		if(type == ENetEventType::ENET_EVENT_TYPE_CONNECT) {
			std::cout << "Server: New client connected " << std::endl;
			
			// Process new client
			m_clientCount++;
			m_highScores.insert({ m_clientCount, 0 });
			m_clientMap.insert({ m_clientCount, p });

			// Send ID back to client
			NewPlayerPacket newPlayerPacket(m_clientCount);
			SendPacketToPeer(m_clientCount, newPlayerPacket);

			
		}
		else if(type == ENetEventType::ENET_EVENT_TYPE_DISCONNECT) {
			std::cout << "Server: A client has disconnected" << std::endl;
			m_clientCount--;
		}
		else if (type == ENetEventType::ENET_EVENT_TYPE_RECEIVE) {
			GamePacket * packet = (GamePacket *)event.packet->data;
			ProcessPacket(packet, peer);
		}
		enet_packet_destroy(event.packet);


	}

	if(m_serverGame->hasLevelLoaded()) {
		loadClientLevels(m_serverGame->getLastLevelLoaded());
	}
	

}

void GameServer::updateGame(float dt) {
	m_serverGame->update(dt);

	auto dynamicObjMap = m_serverGame->getDynamicObjectMap();
	for(auto obj: dynamicObjMap) {
		GameObjectStatePacket statePacket(
			obj.first, 
			obj.second->GetTransform().GetWorldPosition(), 
			obj.second->GetPhysicsObject()->GetLinearVelocity(),
			obj.second->GetTransform().GetWorldOrientation());

		SendGlobalMessage(statePacket);

		//SendPacketToPeer(m_clientCount, statePacket);

	}


	// if player hit the ball
	if(m_raycastData.size() != 0) {
		for(auto data: m_raycastData) {
			m_serverGame->applyPlayerRaycast(data);
		}

		m_raycastData.clear();
	}


}

void GameServer::updateHighscore(int playerID, int score) {
	m_highScores.at(playerID) = score;
}

void GameServer::loadClientLevels(int level) {
	LoadLevelPacket newLevel(level);
	SendGlobalMessage(newLevel);
}

void GameServer::resetHighScores() {
	for(auto player: m_highScores) {
		player.second = 0;
	}

}

void GameServer::printHighscores() {
	for(auto client: m_highScores) {
		std::cout << "High score: " << client.first <<" : "<< client.second << std::endl;
	}

}

bool GameServer::ProcessPacket(GamePacket* packet, int peerID) {
	PacketHandlerIterator firstHandler;
	PacketHandlerIterator lastHandler;

	bool canHandle = GetPacketHandlers(packet->type, firstHandler, lastHandler);

	if (canHandle) {
		for (auto i = firstHandler; i != lastHandler; ++i) {
			i->second->ReceivePacket(packet->type, packet, peerID);

			// high score has been updated
			if(packet->type == HighScore) {
				HighScoreMapPacket highScorePacket(m_highScores);
				SendGlobalMessage(highScorePacket);
			}
		}
		return true;
	}

	std::cout << __FUNCTION__ << " no handler for packet type " << packet->type << std::endl;
	return false;


}

void GameServer::SendPacketToPeer(int client, GamePacket& packet) const {
	ENetPacket* dataPacket = enet_packet_create(&packet, packet.GetTotalSize(), 0);

	if(!m_clientMap.empty()) {
		ENetPeer* peer = m_clientMap.at(client);

		enet_peer_send(peer, 0, dataPacket);

	}
}

void GameServer::initReceivers() {
	BasicReceiver* basicReceiver = new BasicReceiver("Basic_Receiver");
	RegisterPacketHandler(String_Message, basicReceiver);

	HighScoreReceiver* highScoreReceiver = new HighScoreReceiver("HighScore_Receiver", &m_highScores);
	RegisterPacketHandler(HighScore, highScoreReceiver);

	RayCastReceiver* rayCastReceiver = new RayCastReceiver("RayCast_Receiver", &m_raycastData);
	RegisterPacketHandler(RayCast_Force, rayCastReceiver);


}



void GameServer::ThreadedUpdate() {
	while (m_threadAlive) {
		UpdateServer();
		std::this_thread::yield();
	}
}

//Second networking tutorial stuff


void GameServer::BroadcastSnapshot(bool deltaFrame) {

}

void GameServer::UpdateMinimumState() {

}