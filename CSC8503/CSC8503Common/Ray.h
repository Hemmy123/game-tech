#pragma once
#include "../../Common/Vector3.h"
#include "../../Common/Plane.h"

namespace NCL {
	namespace Maths {

		struct RayCollision {
			void*		node;			//Node that was hit (pointer to gameObject)
			Vector3		collidedAt;		//WORLD SPACE position of the collision!
			float		rayDistance;

			RayCollision(void*node, Vector3 collidedAt) {
				this->node			= node;
				this->collidedAt	= collidedAt;
				this->rayDistance	= 0.0f;
			}

			RayCollision() {
				node			= nullptr;
				rayDistance		= FLT_MAX;
			}
		};


		struct RayCastData {
			RayCastData(
				int playerId,
				Vector3 direction,
				Vector3 collidedAt, 
				float forceMag
			) :
				m_playerId(playerId),
				m_direction(direction),
				m_collidedAt(collidedAt),
				m_forceMagnitude(forceMag){};

			RayCastData() {
				m_direction = Vector3();
				m_collidedAt = Vector3();
				m_forceMagnitude = 0;
			}

			int m_playerId;
			Vector3 m_direction;
			Vector3 m_collidedAt;
			float m_forceMagnitude;
		};

		class Ray {
		public:
			Ray(Vector3 position, Vector3 direction) {
				this->position  = position;
				this->direction = direction;
			}
			~Ray(void) {}

			Vector3 GetPosition() const {return position;	}

			Vector3 GetDirection() const {return direction;	}

		protected:
			Vector3 position;	//World space position
			Vector3 direction;	//Normalised world space direction
		};
	}
}