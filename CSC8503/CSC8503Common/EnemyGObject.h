#pragma once
#include "DynamicGObject.h"


using namespace NCL::CSC8503;

class EnemyGObject :
	public DynamicGObject
{
public:
	EnemyGObject();
	virtual ~EnemyGObject();

	void chaseObject(GameObject* c) { m_chase = c; }
	GameObject* getChasing() const { return m_chase; }
	virtual void OnCollisionBegin(GameObject* otherObject) override;

	Vector3 getOriginalPos() const { return m_originalPosition; }
	void  setOriginalPos(Vector3 pos) { m_originalPosition = pos; }

	bool getAlive() const { return m_alive; }
	void setAlive(bool b) { m_alive = b; }


private:
	bool m_alive;
	GameObject* m_chase;
	Vector3 m_originalPosition;
};

