#pragma once
#include <enet/enet.h>
#include <map>
#include <string>
#include <map>
#include "../../Common/Vector3.h"
#include "../../Common/Quaternion.h"


using namespace NCL::Maths;

enum BasicNetworkMessages {
	None,
	Hello,
	Message,
	
	HighScore,
	HighScore_Boardcast,

	New_Level,

	RayCast_Force,

	String_Message,
	Int_Message,
	Delta_State,	//1 byte per channel since the last state
	Full_State,		//Full transform etc
	Received_State, //received from a client, informs that its received packet n
	Player_Connected,
	Player_Disconnected,
	Shutdown
};

struct GamePacket {
	short size;
	short type;

	GamePacket() {
		type		= BasicNetworkMessages::None;
		size		= 0;
	}

	GamePacket(short type) {
		this->type	= type;
	}

	int GetTotalSize() {
		return  sizeof(type) + sizeof(size) + size;
	}
};

struct SingleHighScorePacket: public GamePacket {
	int m_score;
	int m_id;

	SingleHighScorePacket(int id, int score) {
		type = BasicNetworkMessages::HighScore;
		size = sizeof(score) + sizeof(m_id);
		m_score = score;
		m_id = id;
	};
};

struct HighScoreMapPacket: public GamePacket {

	// A large array of ints which will need to be resized later
	int m_highScores[128];
	int m_elems;

	HighScoreMapPacket(std::map<int, int>& scoreMap) {

		type = BasicNetworkMessages::HighScore_Boardcast;
		size = (128) * (sizeof(int)) + sizeof(m_elems);

		int scoreIter	= 0;

		for(auto score: scoreMap) {
			int actualID		= score.first;
			int actualScore		= score.second;
			m_highScores[scoreIter++] = actualID;
			m_highScores[scoreIter++] = actualScore;
		}
		m_elems = scoreIter;
	};
};



struct LoadLevelPacket: public GamePacket {
	int m_level;

	LoadLevelPacket(int i ) {
		size = sizeof(i);
		type = New_Level;
		m_level = i;
	}
};

struct IntPacket : public GamePacket {
	int m_int;

	IntPacket(int i) {
		type = BasicNetworkMessages::Int_Message;
		size = sizeof(i);
		m_int = i;
	};

	int GetValue() {
		return m_int;
	}

};


struct RayCastDataPacket: public GamePacket {

	int m_id;
	Vector3 m_direction;
	Vector3 m_collidedAt;
	float m_forceMagnitude;
	

	RayCastDataPacket() {
		m_id = 0;
		m_direction = Vector3();
		m_collidedAt = Vector3();
		m_forceMagnitude = 0;
	}

	RayCastDataPacket(int id, Vector3 direction, Vector3 collidedAt, float forceMag) {
		type = RayCast_Force;
		size = sizeof(Vector3) * 2 + sizeof(float) + sizeof(int);

		m_id				= id;
		m_direction			= direction;
		m_collidedAt		= collidedAt;
		m_forceMagnitude	= forceMag;
	}


};

struct GameObjectStatePacket: public GamePacket {

	int m_id;
	Vector3 m_position;
	Vector3 m_linearVelocity;
	Quaternion m_orientation;

	GameObjectStatePacket(int id, Vector3 position, Vector3 linearVelocity, Quaternion rotation) {
		type = Full_State;
		size = 
			sizeof(Vector3) * 2 +
			sizeof(Quaternion) +
			sizeof(int);


		m_id = id;
		m_position			= position;
		m_linearVelocity	= linearVelocity;
		m_orientation		= rotation;
	}
};


struct StringPacket : public GamePacket {
	// Char because we need to working with c strings 
	char	stringData[256];

	StringPacket(const std::string& message) {
		type		= BasicNetworkMessages::String_Message;
		size		= (short)message.length();

		memcpy(stringData, message.data(), size);
	};

	std::string GetStringFromData() {
		std::string realString(stringData);
		realString.resize(size);
		return realString;
	}
};

struct NewPlayerPacket : public GamePacket {
	int playerID;
	NewPlayerPacket(int id ) {
		type		= BasicNetworkMessages::Player_Connected;
		playerID	= id;
		size		= sizeof(int);
	}
};

struct PlayerDisconnectPacket : public GamePacket {
	int playerID;
	PlayerDisconnectPacket(int id) {
		type		= BasicNetworkMessages::Player_Disconnected;
		playerID	= id;
		size		= sizeof(int);
	}
};

class PacketReceiver {
public:
	virtual void ReceivePacket(int type, GamePacket* payload, int source = -1) = 0;
};

class NetworkBase	{
public:
	static void Initialise();
	static void Destroy();

	static int GetDefaultPort() {
		return 1234;
	}

	void RegisterPacketHandler(int msgID, PacketReceiver* receiver) {
		m_packetHandlers.insert(std::make_pair(msgID, receiver));
	}

protected:
	NetworkBase();
	~NetworkBase();

	virtual bool ProcessPacket(GamePacket* p, int peerID = -1);

	typedef std::multimap<int, PacketReceiver*>::const_iterator PacketHandlerIterator;

	bool GetPacketHandlers(int msgID, PacketHandlerIterator& first, PacketHandlerIterator& last) const {
		auto range = m_packetHandlers.equal_range(msgID);

		if (range.first == m_packetHandlers.end()) {
			return false; //no handlers for this message type!
		}
		first	= range.first;
		last	= range.second;
		return true;
	}

	//
	ENetHost* m_netHandle;

	std::multimap<int, PacketReceiver*> m_packetHandlers;
};