#pragma once
#include "../../Common/Vector2.h"
#include "Debug.h"
#include <list>
#include <functional>

namespace NCL {
	using namespace NCL::Maths;
	namespace CSC8503 {
		template<class T>
		class QuadTree;

		template<class T>
		struct QuadTreeEntry {
			Vector3 m_pos;
			Vector3 m_size;
			T		m_object;
			QuadTreeEntry(T obj, Vector3 pos, Vector3 size) {
				m_object = obj;
				m_pos	= pos;
				m_size	= size;
			}

		};


		template<class T>
		class QuadTreeNode	{
		public:
			typedef std::function<
				void(std::list<QuadTreeEntry<T>>&)> QuadTreeFunc;
		protected:
			friend class QuadTree<T>;

			QuadTreeNode() {}

			QuadTreeNode(Vector2 pos, Vector2 size) {
				m_children	= nullptr;
				m_position	= pos;
				m_size		= size;
			}

			~QuadTreeNode() {
				delete[] m_children;
			}

			void Insert(T& object, const Vector3& objectPos, const Vector3& objectSize, int depthLeft, int maxSize) {
				if(!CollisionDetection::AABBTest(
					objectPos, 
					Vector3(m_position.x, 0, m_position.y),
					objectSize,
					Vector3(m_size.x, 1000.0f, m_size.y))) { return; }

				if(m_children) {
					for(int i = 0; i < 4; ++i) {
						m_children[i].Insert(object, objectPos, objectSize, depthLeft - 1, maxSize);
					}
				}
				else {
					m_contents.push_back(QuadTreeEntry<T>(object, objectPos, objectSize));
					if ((int)m_contents.size() > maxSize && depthLeft > 0) {

						if(!m_children) {
							Split();

							for(const auto&i : m_contents) {
								for (int j = 0; j < 4;  ++j) {
									auto entry = i;
									m_children[j].Insert(entry.m_object, entry.m_pos, entry.m_size, depthLeft - 1, maxSize);
								}
							}
							m_contents.clear();
						}


					}
				}



			}

			void Split() {
				Vector2 halfSize = m_size / 2.0f;
				m_children = new QuadTreeNode<T>[4];

				m_children[0] = QuadTreeNode<T>(m_position + Vector2(-halfSize.x, halfSize.y), halfSize);
				m_children[1] = QuadTreeNode<T>(m_position + Vector2(halfSize.x, halfSize.y), halfSize);
				m_children[2] = QuadTreeNode<T>(m_position + Vector2(-halfSize.x, -halfSize.y), halfSize);
				m_children[3] = QuadTreeNode<T>(m_position + Vector2(halfSize.x, -halfSize.y), halfSize);




			}

			void DebugDraw() {

				//Debug::DrawLine(m_position, m_size, Vector4(1, 0, 1, 1));

			}

			void OperateOnContents(QuadTreeFunc& func) {
				if(m_children) {
					for(int i = 0; i < 4; ++i) {
						m_children[i].OperateOnContents(func);
					}
				} else {
					if(!m_contents.empty()) {
						func(m_contents);
					}
				}

			}

		protected:
			std::list<QuadTreeEntry<T>>	m_contents;

			Vector2 m_position;
			Vector2 m_size;

			QuadTreeNode<T>* m_children;
		};
	}
}


namespace NCL {
	using namespace NCL::Maths;
	namespace CSC8503 {
		template<class T>
		class QuadTree	{
		public:
			QuadTree(Vector2 size, int maxDepth = 6, int maxSize = 5) 
			: m_root(QuadTreeNode<T>(Vector2(), size)){
				m_maxDepth = maxDepth;
				m_maxSize = maxSize;
			}
			~QuadTree() { }

			void Insert(T object, const Vector3& pos, const Vector3& size) {
				m_root.Insert(object, pos, size, m_maxDepth, m_maxSize);
			}

			void DebugDraw() {
				m_root.DebugDraw();

			}

			void OperateOnContents(
				typename QuadTreeNode<T>::QuadTreeFunc  func) {
				m_root.OperateOnContents(func);
			}

		protected:
			QuadTreeNode<T> m_root;
			int m_maxDepth;
			int m_maxSize;
		};
	}
}