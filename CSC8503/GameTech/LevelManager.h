#pragma once
#include "../CSC8503Common/StateMachine.h"

using namespace NCL::CSC8503;


class LevelManager
{
public:
	LevelManager();
	~LevelManager();

	void initStates();
	void initTransitions();
private:
	int m_currentLevel;
	StateMachine* m_FSM;
};

