#include "LevelLoader.h"
#include "../CSC8503Common/PositionConstraint.h"

using std::vector;
using std::string;

LevelLoader::LevelLoader(GameWorld* world):
m_world(world)
{

	m_objectCreator = new GameObjectCreator();


}


LevelLoader::~LevelLoader()
{
	

}

std::vector<GameObject*> LevelLoader::readAndParseFile(std::string file) {
	clearLoader();



	if (!m_reader.startReader(file)) {
		return m_parsedObjects;
	}

	readXandY();
	createFloor();
	vector<string> readFile = m_reader.readRestOfFile();

	parseFileStringToGrid(readFile);
	parseGridToGameObj();

	m_reader.endReader();
	return m_parsedObjects;
	
}

void LevelLoader::clearLoader() {
	m_ids = 0;
	m_playerObjects.clear();
	m_enemyObjects.clear();
	m_goalObjects.clear();
	m_parsedObjects.clear();
	m_spinningObjects.clear();
	m_maceObjects.clear();
	m_dynamicObjects.clear();

}

Tile LevelLoader::charToTile(char c) {

	switch(c) {
	case 'H': return Tile::HorizonalWall;
	case 'V': return Tile::VerticalWall;
	case 'P': return Tile::Player;
	case 'G': return Tile::Goal;
	case 'W': return Tile::Windmill;
	case 'E': return Tile::Enemy;
	case 'M': return Tile::Mace;
	case '.': return Tile::Air;

	default: return Tile::Unknown;
	}

}

void LevelLoader::readXandY() {

	// Read the first 2 lines
	m_xSize = std::stoi(m_reader.readLine());
	m_ySize = std::stoi(m_reader.readLine());

	//m_grid = std::vector<std::vector<Tile>>(m_xSize, vector<Tile>(m_ySize, Tile::Unknown) );
	m_grid = std::vector<std::vector<Tile>>(m_ySize, vector<Tile>(m_xSize, Tile::Unknown) );

}

void LevelLoader::parseFileStringToGrid(std::vector<std::string> fileString) {
	for (unsigned x = 0; x < fileString.size(); ++x) {
		string line = fileString[x];
		line.erase(std::remove(line.begin(), line.end(), ','), line.end()); // strips commas
		for (unsigned y = 0; y < line.size(); ++y) {
			Tile t = charToTile(line[y]);
			if (t == Tile::Unknown) std::cout << "Tile unknown!!" << std::endl;
			m_grid[x][y] = t;
		}
	}

}

void LevelLoader::parseGridToGameObj() {
 	for (unsigned y = 0; y < m_grid.size(); ++y) {
		vector<Tile> row = m_grid[y];

		for (unsigned x = 0; x < row.size(); ++x) {
			Tile currentTile = row[x];

			Vector3 cubePos = Vector3(x * m_wallHalfSize.x * 2, m_levelHeight, y * m_wallHalfSize.z * 2);
			Quaternion defaultRotation;

			switch (currentTile) {
			case Tile::HorizonalWall: {
				GameObject* tempBox = m_objectCreator->createStaticBox(cubePos, m_wallHalfSize, GameObjectType::StaticBox, defaultRotation);
				tempBox->setId(m_ids++);
				m_parsedObjects.push_back(tempBox);

				break;
			}
			case Tile::VerticalWall: {
				GameObject* tempBox = m_objectCreator->createStaticBox(cubePos, m_wallHalfSize, GameObjectType::StaticBox, defaultRotation);
				tempBox->setId(m_ids++);

				m_parsedObjects.push_back(tempBox);

				break;
			}

			case Tile::Player: {
				GameObject* player = m_objectCreator->createPlayer(cubePos, m_playerRadius, 1, GameObjectType::Player);
				player->setId(m_ids++);

				m_parsedObjects.push_back(player);
				PlayerGObject* playerObj = (PlayerGObject*)player;
				playerObj->setInitialPosition(player->GetTransform().GetWorldPosition());
				m_playerObjects.push_back((PlayerGObject*)playerObj);
				m_dynamicObjects.push_back(player);

				break;
			}
			case Tile::Goal: {
				GameObject* goalBox = m_objectCreator->createStaticBox(cubePos, m_wallHalfSize, GameObjectType::Goal, defaultRotation );
				goalBox->setId(m_ids++);

				m_parsedObjects.push_back(goalBox);
				m_goalObjects.push_back(goalBox);
				break;
			}


			case Tile::Windmill: {
				Vector3 windmillHalfSize(8.0f, 1.0f, 1.0f);;

				GameObject* windMill = m_objectCreator->createSpinningObject(cubePos, windmillHalfSize,  3, RotationOrientation::Horizontal, GameObjectType::Windmill);
				windMill->setId(m_ids++);
				m_parsedObjects.push_back(windMill);
				m_spinningObjects.push_back((SpinningGObject*)windMill);
				m_dynamicObjects.push_back(windMill);

				break;
			}

			case Tile::Enemy: {
				EnemyGObject* enemyObject = (EnemyGObject*)m_objectCreator->createEnemy(cubePos, m_wallHalfSize, 1);
				enemyObject->setId(m_ids++);
				enemyObject->setOriginalPos(enemyObject->GetTransform().GetWorldPosition());
				m_parsedObjects.push_back(enemyObject);
				m_enemyObjects.push_back(enemyObject);
				m_dynamicObjects.push_back(enemyObject);
				break;
			}
			case Tile::Mace: {
				cubePos.y = m_levelHeight + 30;
				Vector3 maceSize(1, 1, 1);
				PatrollingGObject* mace = createMace(cubePos, maceSize, 10);
				mace->setId(m_ids++);
				mace->setSpeed(0.5);
				mace->initStates();
				m_maceObjects.push_back(mace);
				
				break;
			}

			case Tile::Air: {
				break;
			}
			default: break;
			}


		}
	}
}

void LevelLoader::createFloor() {
	float xHalfSize = (m_xSize * m_wallHalfSize.x*2)/2;
	float zHalfSize = (m_ySize * m_wallHalfSize.x*2)/2;
	float yHalfSize = (10.0f)/2.0f;


	float height = m_levelHeight - m_wallHalfSize.y*2 ;

	Vector3 pos(xHalfSize - m_wallHalfSize.x, height, zHalfSize - m_wallHalfSize.z);

	Vector3 size(xHalfSize, yHalfSize, zHalfSize);

	GameObject* floor = m_objectCreator->createStaticBox(pos, size, GameObjectType::Floor);
	floor->GetPhysicsObject()->setFriction(0.8f);
	//floor->GetRenderObject()->SetDefaultTexture();
	m_parsedObjects.push_back(floor);


}

void LevelLoader::parseHorizontalWalls() {

	// for every row in the grid;
	//for (unsigned y = 0; y < m_grid.size(); ++y) {
	//	vector<Tile> row = m_grid[y];

	//	for (unsigned x = 0; x < row.size(); ++y) {
	//		Tile currentTile = row[x];

	//		// go across and search 
	//		if(currentTile == Tile::HorizonalWall) {
	//			
	//		}
	//		
	//	}
	//}

}

void LevelLoader::parseVerticalWalls() {


}

PatrollingGObject* LevelLoader::createMace(Vector3 handlePos, Vector3 halfSize, float chainMass) {

	float sizeMultiplier = 1.0f;

	Vector3 cubeSize = halfSize * sizeMultiplier;

	int numLinks = 5;
	float distance = 4.0f;

	PatrollingGObject* handle = m_objectCreator->createPatrollingObject(handlePos, halfSize, GameObjectType::Mace,0);
	m_parsedObjects.push_back(handle);
	m_dynamicObjects.push_back(handle);
	GameObject* previous = handle;

	for (int i = 0; i < numLinks; ++i) {
		Vector3 blockPos(handlePos.x, handlePos.y - (i + 1) * distance , handlePos.z);
		GameObject* block = m_objectCreator->createStaticBox(blockPos, cubeSize, GameObjectType::Mace, Quaternion());
		
		block->setId(m_ids++);
		m_parsedObjects.push_back(block);
		m_dynamicObjects.push_back(block);
		block->GetPhysicsObject()->SetInverseMass(1 / chainMass);
		PositionConstraint* constraint = new PositionConstraint(previous, block, distance);
		m_world->AddConstraint(constraint);
		previous = block;
	}

	return handle;
}
