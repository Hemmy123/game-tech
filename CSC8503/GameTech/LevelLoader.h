#pragma once
#include "FileReader.h"
#include "../CSC8503Common/GameObject.h"
#include "../CSC8503Common/PlayerGObject.h"
#include "../CSC8503Common/DynamicGObject.h"
#include "../CSC8503Common/StaticGObject.h"
#include "GameObjectCreator.h"
#include <vector>
#include <algorithm>
#include "../CSC8503Common/PatrollingGObject.h"
using namespace NCL::CSC8503;

enum class Tile {
	HorizonalWall,
	VerticalWall,
	Player,
	Enemy,
	Goal,
	Windmill,
	Mace,


	Air,
	Water,

	// -- //
	Unknown,
};

/**
 * Loads a level from a text file using a file reader
 */
class LevelLoader
{
public:
	LevelLoader(GameWorld* world);
	~LevelLoader();

	std::vector<GameObject*> readAndParseFile(std::string file);

	void clearParsedObjects() { m_parsedObjects.clear(); }


	std::vector<GameObject*>			getParsedObjects()		const { return m_parsedObjects; };
	std::vector<PlayerGObject*>			getPlayerObjects()		const { return m_playerObjects; }
	std::vector<GameObject*>			getGoalObjects()		const { return m_goalObjects; }
	std::vector<EnemyGObject*>			getEnemyObjects()		const { return m_enemyObjects; }
	std::vector<SpinningGObject*>		getSpinningObjects()	const { return m_spinningObjects; }
	std::vector<PatrollingGObject*>		getMaceObjects()		const { return m_maceObjects; }
	std::vector<GameObject*>			getDynamicObjects()		const { return m_dynamicObjects; }
	
	


private:

	int m_ids;

	void clearLoader();

	Tile charToTile(char c);

	/// Reads the first 2 lines in the 
	/// file and puts them in xSize and ySize
	void readXandY();

	/// Parses a vector of strings to a 2D vector of
	/// chars which represents the map
	void parseFileStringToGrid(std::vector<std::string> fileString);

	/// parses a grid into gameObjects;
	void parseGridToGameObj();

	void createFloor();

	/// ----- Merging multiple boxes into one long box ----- ///

	/// looks through the char grid and merges horizontal
	/// boxes into one long box.
	void parseHorizontalWalls();

	/// looks through the char grid and merges vertical
	/// boxes into one long box
	void parseVerticalWalls();

	PatrollingGObject* createMace(Vector3 handlePos, Vector3 halfSize, float chainMass);

	FileReader m_reader;
	GameObjectCreator* m_objectCreator;
	
	std::vector<GameObject*> m_parsedObjects;

	// ----- For special game interactions ----- //
	std::vector<PlayerGObject*>		m_playerObjects;
	std::vector<EnemyGObject*>		m_enemyObjects;
	std::vector<GameObject*>		m_goalObjects;
	std::vector<SpinningGObject*>	m_spinningObjects;

	// Any objects that can move;
	std::vector<GameObject*>		m_dynamicObjects;

	// Each object is the "handle" of the mace, where the "chain"
	// is a series of Cubes/Spheres
	std::vector<PatrollingGObject*>		m_maceObjects;


	int m_xSize;
	int m_ySize;

	Vector3 m_wallHalfSize	= Vector3(5.0f, 5.0f, 5.0f);
	float m_levelHeight		= 100.0f;
	float m_playerRadius	= 5.0f;
	float m_playerMass		= 1.0f;

	std::vector<std::vector<Tile>> m_grid;

	GameWorld* m_world;




};

