#include "NavigationManager.h"
#include "../CSC8503Common/Debug.h"


NavigationManager::NavigationManager()
{
	m_pathFound = false;


}


NavigationManager::~NavigationManager()
{
}

void NavigationManager::update() {
	m_navigator.update();
}

void NavigationManager::loadNewNavigationGrid(std::string fileName) {
	if (m_navigationGrid) {
		delete m_navigationGrid;
	}
	m_navigationGrid = new NavigationGrid(fileName, m_cubeScale);
	m_navigator.update();

}

bool NavigationManager::findPath(Vector3 startPos, Vector3 endPos) {

	m_startPos	= startPos;
	m_endPos	= endPos;

	m_pathFound  = m_navigationGrid->FindPath(startPos, endPos, m_navigationPath);

	Vector3 pos;
	m_vectorPath.clear();
	while (m_navigationPath.PopWaypoint(pos)) {
		m_vectorPath.push_back(pos);
	}
	m_navigator.setPath(m_vectorPath);
	return m_pathFound;


}

void NavigationManager::displayPathfinding() {

	for (int i = 1; i < m_vectorPath.size(); ++i) {
		Vector3 a = m_vectorPath[i - 1];
		Vector3 b = m_vectorPath[i];

		Vector3 newA(a.x, 100, a.z);
		Vector3 newB(b.x, 100, b.z);

		NCL::Debug::DrawLine(newA, newB, Vector4(0, 1, 0, 1));
	}

}

void NavigationManager::setObject(GameObject* obj) {
	m_navigator.setObject(obj);
	
}
