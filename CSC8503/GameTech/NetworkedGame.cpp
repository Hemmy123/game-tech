

#include "NetworkedGame.h"
#include "NetworkPlayer.h"
#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"
#include "../CSC8503Common/PacketReceivers.h"

#define COLLISION_MSG 30

::NetworkedGame::NetworkedGame(PacketReceiver* packetReceiver):
	m_connected(false),
	m_packetReceiver(packetReceiver) {
	m_client = new GameClient();
	m_client->RegisterPacketHandler(String_Message, m_packetReceiver);
	m_clientID = 0;
	initReceivers();

	setIsServer(false);
	setMultiplayer(true);

	m_globalHighScores.insert({ 1,0 });
	m_globalHighScores.insert({ 2,0 });
	m_globalHighScores.insert({ 3,0 });
	m_globalHighScores.insert({ 4,0 });

	m_begin = false;
	m_newLevelReceived = false;

	m_multiplayer = true;
	m_isServer = false;


}

::NetworkedGame::~NetworkedGame() {


}

void NetworkedGame::update(float dt) {
	if(m_begin) {
		
		updateClient();
		UpdateGame(dt);

		if (didPlayerHitBall()) {
			RayCastDataPacket rayData = getRayCastData();
			sendPacket(rayData);
		}


	} else {
		m_client->UpdateClient();
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		if(m_newLevelReceived) {
			m_begin = true;
			loadLevel(m_currentLevel);
		}
	}
}

void ::NetworkedGame::updateClient() {
	m_client->UpdateClient();

	// if there's a position update
	if(m_client->getPositionUpdated()) {

		updateReceivedState(m_client->getUpdatedStates());
		m_client->clearStates();
	}

	if(getScoreUpdated()) {
		int score = getScore();

		SingleHighScorePacket highScoreMessage(m_clientID, score);
		sendPacket(highScoreMessage);

	}

	checkForNewLevel();

	displayerHighScores(m_globalHighScores);

}

void NetworkedGame::updateReceivedState(std::vector<GameObjectState> states) {
	for(auto state: states) {
		updateDynamicObject(state.m_id, state);
	}

}

void NetworkedGame::checkForNewLevel() {

	if (m_newLevelReceived) {
		loadLevel(m_currentLevel);
		m_newLevelReceived = false;
	}
}

void NetworkedGame::initReceivers() {
	BasicReceiver*		basicReceiver		= new BasicReceiver("Basic_Receiver");
	IntReceiver*		idReceiver			= new IntReceiver("Receive_ID", m_clientID);
	HighScoreReceiver*	highScoreReceiver	= new HighScoreReceiver("HighScore_Receiver", &m_globalHighScores);
	StateReceiver*		stateReceiver		= new StateReceiver("State_Receiver", m_updatedState);
	LevelReceiver*		levelReceiver		= new LevelReceiver("Level_Receiver", m_currentLevel, m_newLevelReceived);

	m_client->RegisterPacketHandler(Full_State, stateReceiver);
	m_client->RegisterPacketHandler(String_Message, basicReceiver);
	m_client->RegisterPacketHandler(Player_Connected, idReceiver);
	m_client->RegisterPacketHandler(HighScore_Boardcast, highScoreReceiver);
	m_client->RegisterPacketHandler(New_Level, levelReceiver);

}

bool NetworkedGame::connectToServer(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum) {
	m_connected = m_client->Connect(a, b, c, d, portNum);
	return m_connected;
}

void NetworkedGame::sendPacket(GamePacket& packet) const {
	m_client->SendPacket(packet);
}
