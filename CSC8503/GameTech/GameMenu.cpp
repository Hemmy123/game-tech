#include "GameMenu.h"
#include "../CSC8503Common/Debug.h"


GameMenu::GameMenu(GameTechRenderer* renderer): 
m_selectedLevel(0),
m_renderer(renderer){

	m_type = MenuType::Intro;
}


GameMenu::~GameMenu()
{
}

void GameMenu::update() {

	displayMenu(m_type);

}

void GameMenu::displayMenu(MenuType menu) {


	switch(menu) {
	case MenuType::Intro: introMenu(); break;
	case MenuType::Pause: pauseMenu(); break;
	case MenuType::End: endMenu(); break;
	}
}

void GameMenu::introMenu() {

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_1)) {
		m_selectedLevel = 1;
	}
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_2)) {
		m_selectedLevel = 2;
	}
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_3)) {
		m_selectedLevel = 3;
	}
	m_unpausedPressed = 0;
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_U)) {
		m_unpausedPressed = (int)'u';
	}

	float offset = 20;
	float yPos = 100;
	Vector2 textPos1(10, yPos);
	Vector2 textPos2(10, yPos - offset);
	Vector2 textPos3(10, yPos - offset * 2);
	Vector2 textPos4(10, yPos - offset * 3);


	Debug::Print("Press 1 to load level 1", textPos1);
	Debug::Print("Press 2 to load level 2", textPos2);
	Debug::Print(" \"m\" to pause", textPos3);
	Debug::Print(" \"u\" to unpause", textPos4);
	Debug::FlushRenderables();

	m_renderer->Render();

}

void GameMenu::pauseMenu() {

	m_unpausedPressed = 0;
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_U)) {
		m_unpausedPressed = (int)'u';
	}

	float offset = 20;
	float yPos = 100;
	Vector2 textPos1(10, yPos);
	Vector2 textPos2(10, yPos - offset);

	Debug::Print(" \"m\" to pause", textPos1);
	Debug::Print(" \"u\" to unpause", textPos2);
	Debug::FlushRenderables();

	m_renderer->Render();
}

void GameMenu::endMenu() {
	m_unpausedPressed = 0;
	/*if (Window::GetKeyboard()->KeyPressed(KEYBOARD_U)) {
		m_unpausedPressed = (int)'u';
	}*/

	float offset = 20;
	float yPos = 100;
	Vector2 textPos1(10, yPos);
	Vector2 textPos2(10, yPos - offset);

	Debug::Print("You win!", textPos1);
	//Debug::Print(" \"u\" to unpause", textPos2);
	Debug::FlushRenderables();

	m_renderer->Render();

}
