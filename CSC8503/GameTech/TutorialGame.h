

#pragma once
#include "GameTechRenderer.h"
#include "../CSC8503Common/PhysicsSystem.h"

namespace NCL {
	namespace CSC8503 {
		class TutorialGame		{
		public:
			TutorialGame();
			virtual ~TutorialGame();

			virtual void UpdateGame(float dt);


			GameTechRenderer* getRenderer() const { return m_renderer; }


		protected:
			void InitWorld();


			void InitialiseAssets();

			void InitCamera();
			void UpdateKeys();


			/*
			These are some of the world/object creation functions I created when testing the functionality
			in the module. Feel free to mess around with them to see different objects being created in different
			test scenarios (constraints, collision types, and so on). 
			*/
			void InitSphereGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, float radius);
			void InitMixedGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing);
			void InitCubeGridWorld(int numRows, int numCols, float rowSpacing, float colSpacing, const Vector3& cubeDims);
			void InitSphereCollisionTorqueTest();
			void InitCubeCollisionTorqueTest();
			void InitSphereAABBTest();
			void InitGJKWorld();
			void BridgeConstraintTest();
			void SimpleGJKTest();
			void SimpleAABBTest();
			void SimpleAABBTest2();

			bool SelectObject();
			void MoveSelectedObject();

			GameObject* AddFloorToWorld(const Vector3& position);
			GameObject* AddSphereToWorld(const Vector3& position, float radius, float inverseMass = 10.0f);
			GameObject* AddCubeToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f);

			GameObject* AddOBBToWorld(const Vector3& position, Vector3 dimensions, float inverseMass = 10.0f);


			GameTechRenderer*	m_renderer;
			PhysicsSystem*		m_physics;
			GameWorld*			m_world;

			bool useGravity;
			bool inSelectionMode;

			float		forceMagnitude;

			GameObject* m_selectionObject = nullptr;

			OGLMesh*	m_cubeMesh		= nullptr;
			OGLMesh*	m_sphereMesh	= nullptr;
			OGLTexture* m_basicTex		= nullptr;
			OGLShader*	m_basicShader	= nullptr;

			// ----- Game Mode ----- // 
			bool m_debugMode;

		};
	}
}

