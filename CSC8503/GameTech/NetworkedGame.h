

#pragma once
#include "Game.h"
#include "../CSC8503Common/GameClient.h"
#include "GameStateManager.h"
#include "../CSC8503Common/GameObject.h"

using namespace NCL::CSC8503;
/*
 * A Client game that connects to a server
 *
 */
//class NetworkedGame : public GameStateManager {
class NetworkedGame : public Game {
public:
	NetworkedGame(PacketReceiver* packetReceiver);
	~NetworkedGame();


	void update(float dt);

	bool connectToServer(uint8_t a, uint8_t b, uint8_t c, uint8_t d, int portNum);

	void sendPacket(GamePacket& packet) const;

	void updateClient();

	bool isConnected() const { return m_connected; }

	int getID() const { return m_clientID; }


private:


	void initClient();
	void initReceivers();
	void updateReceivedState(std::vector<GameObjectState> states);

	void checkForNewLevel();
	std::map<int, int> m_globalHighScores;

	// a new state that has been updated by the server
	GameObjectState m_updatedState;

	int m_clientID;

	int m_currentLevel;
	bool m_newLevelReceived;


	bool m_connected;

	bool m_begin;


	PacketReceiver* m_packetReceiver;
	GameClient* m_client;
};

