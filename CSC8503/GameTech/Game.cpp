

#include "Game.h"
#include "../../Common/Assets.h"
#include "../CSC8503Common/State.h"
#include "../CSC8503Common/PatrollingGObject.h"


Game::Game(int id)
{
	m_isServer		= false;
	m_multiplayer	= false;
	m_objectCreator = new GameObjectCreator();
	m_cubeScale		= 10.0f;
	m_currentLevel	= 0;
	m_finishedLevel = false;
	m_finishedGame	= false;
	m_lastLevel		= 2;
	m_loader		= new LevelLoader(m_world);
	m_patrolSpeed	= 1;
	m_scoreUpdated	= false;
	m_loadNewClientLevel = false;
}


Game::~Game()
{
}

void Game::initScores() {


}

void Game::loadLevel(int level) {
	resetLevel();
	m_navigationManager.clearNavigator();

	int minSpeed = 30;
	int maxSpeed = 40;


	switch(level) {
	case 1: {
		readLevelFile("LevelOne.csv");
		m_currentLevel = 1;
		
		for(auto mace: m_maces){
			Vector3 oldTrans = mace->GetTransform().GetWorldPosition();
			mace->setPosA(oldTrans);
			mace->setPosB(Vector3(oldTrans.x, oldTrans.y, 140));
			mace->setSpeed(rand() % minSpeed + maxSpeed);
		}

		break;
	}; 
	case 2: {
		readLevelFile("LevelTwo.csv");
		m_currentLevel = 2;


		for (auto mace : m_maces) {
			Vector3 oldTrans = mace->GetTransform().GetWorldPosition();
			mace->setPosA(oldTrans);
			mace->setPosB(Vector3(oldTrans.x +  50, oldTrans.y, oldTrans.z));
			mace->setSpeed(rand() % minSpeed + maxSpeed);

		}

		break;
	}

	default: {
		readLevelFile("LevelOne.csv");
		break;
	}
	}
	if(m_enemyObjects.size() != 0) {
		m_navigationManager.setObject(m_enemyObjects[0]);
	}
}

void Game::movePlayer(PlayerGObject* playerObj) {
	m_renderer->DrawString("Click Force:" + std::to_string(forceMagnitude), Vector2(10, 20));
	forceMagnitude += Window::GetMouse()->GetWheelMovement() * 1000.0f;

	if (!playerObj) {
		return;//we haven't selected anything!
	}
	//Push the selected object!
	if (Window::GetMouse()->ButtonPressed(NCL::MouseButtons::MOUSE_LEFT)) {

		Ray ray = CollisionDetection::BuildRayFromMouse(*m_world->GetMainCamera());

		RayCollision closestCollision;
		if (m_world->Raycast(ray, closestCollision, true)) {
			if (closestCollision.node == playerObj) {
				m_score++;
				m_scoreUpdated = true;
				Vector3 direction = ray.GetDirection();
				Vector3 collidedAt = closestCollision.collidedAt;
				float force = forceMagnitude;
				
				playerObj->GetPhysicsObject()->AddForceAtPosition(ray.GetDirection() * forceMagnitude, closestCollision.collidedAt);

				m_playerRaycasted = true;
				m_playerRaycastData = RayCastDataPacket(playerObj->getId(),direction, collidedAt, force);

			}
		}
	}

}

void Game::loadLevelObj(Level* level) {
	m_selectionObject = nullptr;

	for(auto gameObject: level->getGameObjects()) {
		m_world->AddGameObject(gameObject);
	}

}

void Game::resetLevel() {

	m_selectionObject = nullptr;
	m_finishedLevel = false;

	m_world->ClearAndErase();

	resetPlayerScore();
	resetGoalReached();
	clearGameObjects();
	m_physics->Clear();
}

void Game::UpdateGame(float dt) {
	m_scoreUpdated = false;
	m_playerRaycasted = false;
	UpdateKeys();
	updateGameKeys();
	m_navigationManager.update();
	spinObjects();
	patrolMace();

	
	if (m_debugMode) {

		SelectObject();
		MoveSelectedObject();
		if (!inSelectionMode) {
			m_world->GetMainCamera()->UpdateCamera(dt);
		}
	} else {

		if(Window::GetMouse()->ButtonHeld(MOUSE_RIGHT)) {
			m_world->GetMainCamera()->UpdateCamera(dt);
			Window::GetWindow()->ShowOSPointer(false);
			Window::GetWindow()->LockMouseToWindow(true);

		} else {
			Window::GetWindow()->ShowOSPointer(true);
			Window::GetWindow()->LockMouseToWindow(false);
			m_selectionObject = m_playerObjects[0];
			if(!m_isServer) {
				movePlayer(m_playerObjects[0]);
			}
		//	m_world->GetMainCamera()->followPos(m_playerObjects[0]->GetTransform().GetWorldPosition());
		}
	}




	if(m_enemyObjects.size() != 0 || m_playerObjects.size() != 0) {
		Vector3 enemyPos = m_enemyObjects[0]->GetConstTransform().GetWorldPosition();
		Vector3 playerPos = m_playerObjects[0]->GetConstTransform().GetWorldPosition();
		m_navigationManager.findPath(enemyPos, playerPos);
		m_navigationManager.displayPathfinding();


		printScore();

		if(m_playerObjects[0]->getReachedGoal()) {
			m_finishedLevel = true;
		}

		if(m_playerObjects[0]->isAlive() == false) {
			respawnPlayer(m_playerObjects[0]);
			m_playerObjects[0]->setAlive(true);
		}
	}


	checkRespawn();

	if(m_finishedLevel) {
		m_loadNewClientLevel = true;
		int nextLevel = m_currentLevel + 1;
		if(nextLevel > m_lastLevel) {
			std::cout << "end of the game!";
			m_finishedGame = true;
		} else {
			m_currentLevel = nextLevel;
			loadLevel(m_currentLevel);
		}
	} else if (m_finishedLevel && m_multiplayer && !m_isServer) {
		Vector2 pos(10, 300);
		Debug::Print("Well Done! Waiting for server to start the new level... ", pos);
	}



	m_world->UpdateWorld(dt);
	m_renderer->Update(dt);

	// Server
	if(m_isServer && m_multiplayer) {
		m_physics->Update(dt);
	}
 
	// Client
	if(!m_isServer && m_multiplayer) {
		m_physics->Update(dt, 1);
	}


	// Single player
	if(!m_multiplayer) {
		m_physics->Update(dt);
	}
	
	//m_physics->Update(dt);

	Debug::FlushRenderables();
	m_renderer->Render();
}

void Game::applyRaycastToPlayer(const RayCastData& data) {


	auto iter = m_dynamicObjectMap.find(data.m_playerId);

	if( iter != m_dynamicObjectMap.end()) {
		iter->second->GetPhysicsObject()->AddForceAtPosition(
			data.m_direction * data.m_forceMagnitude,
			data.m_collidedAt);

	}



}

void Game::clearGameObjects() {
	m_playerObjects.clear();
	m_enemyObjects.clear();
	m_goalObjects.clear();
	m_spinningObjects.clear();
	m_maces.clear();
	m_dynamicObjects.clear();
	m_dynamicObjectMap.clear();
}

void Game::resetGoalReached() {

	for (auto gameObj : m_playerObjects) {
		PlayerGObject* player = (PlayerGObject*)gameObj;
		player->setReachedGoal(false);
	}
}


void Game::printScore() {
	int score = m_score;
	Debug::Print("Score: " + std::to_string(score), Vector2(10, 650));
}

void Game::respawnPlayer(PlayerGObject* player) {
	Vector3 initialPos = player->getInitialPosition();
	player->GetPhysicsObject()->SetAngularVelocity(Vector3(0, 0, 0));
	player->GetPhysicsObject()->SetLinearVelocity(Vector3(0, 0, 0));
	player->setPosition(initialPos);
}

void Game::resetPlayerScore() {
	m_score = 0;
}

void Game::updateGameKeys() {

	m_pausedPressed = 0;
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_M)) {
		m_pausedPressed = (int)'m';
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_R)) {

		loadLevel(m_currentLevel);
	}


}

void Game::checkRespawn() {

	if (m_playerObjects.size() != 0) {

		if (m_playerObjects[0]->GetTransform().GetWorldPosition().y < m_killPlane) {
			respawnPlayer(m_playerObjects[0]);
		}

	}

}

void Game::spinObjects() {
	for(auto spinningObject: m_spinningObjects) {
		Vector3 direction;
		float speed = spinningObject->getSpeed();
		if(spinningObject->getOrientation() == RotationOrientation::Horizontal) {
			direction = Vector3(0, speed, 0);
		}
		else if (spinningObject->getOrientation() == RotationOrientation::Vertical) {
			direction = Vector3(0, 0, speed);
		}

		if(direction != Vector3(0,0,0)) {
			spinningObject->GetPhysicsObject()->SetAngularVelocity(direction);
		}

	}

}

void Game::patrolMace() {
	for(auto mace: m_maces) {
		mace->update();
	}
}


void Game::displayText() {
	Vector2 textPos(10, 40);
	if (useGravity) {
		Debug::Print("(G)ravity on", textPos);
	}
	else {
		Debug::Print("(G)ravity off", textPos);
	}

}

void Game::displayerHighScores(std::map<int, int> highScores) {
	Vector2 pos(10, 600);

	Debug::Print("Player|Score", pos);

	int i = 1;
	for(auto iter: highScores) {
		string msg = "   " + std::to_string(iter.first) + "  |  " + std::to_string(iter.second);
		Vector2 newPos(pos.x, pos.y - (i *15));
		i++;
		Debug::Print(msg, newPos);
	}


}

void Game::updateDynamicObject(int id, const GameObjectState& newState) {

	for(auto gameObject: m_dynamicObjects) {
		if(gameObject->getId() == newState.m_id) {
			gameObject->GetTransform().SetWorldPosition(newState.m_position);
			gameObject->GetTransform().SetLocalOrientation(newState.m_orientation);
			gameObject->GetPhysicsObject()->SetLinearVelocity(newState.m_linearVelocity);

		}
	}

}

void Game::readLevelFile(std::string filePath) {


	m_world->ClearAndErase();
	m_physics->Clear();
	m_loader->clearParsedObjects();


	Level l;
 	l.setGameObjects( m_loader->readAndParseFile(NCL::Assets::LEVELDIR + filePath) );


	m_navigationManager.loadNewNavigationGrid(filePath);
	m_goalObjects		= m_loader->getGoalObjects();
	m_playerObjects		= m_loader->getPlayerObjects();
	m_spinningObjects	= m_loader->getSpinningObjects();
	m_enemyObjects		= m_loader->getEnemyObjects();
	m_maces				= m_loader->getMaceObjects();
	m_dynamicObjects	= m_loader->getDynamicObjects();

	for(auto obj: m_dynamicObjects) {
		m_dynamicObjectMap.insert({ obj->getId(), obj });
	}

	m_score = 0;

	loadLevelObj(&l);
}
