#pragma once
#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/State.h"
#include "../CSC8503Common/StateTransition.h"
#include "GameMenu.h"
#include "Game.h"


using namespace NCL::CSC8503;
class GameStateManager
{
public:
	GameStateManager();
	~GameStateManager();

	void update(float dt);


	float getDT() const { return m_dt; }


	int getScore() const { return m_game->getScore(); }

	void setMultiplayer(bool b) { m_game->setMultiplayer(b); }
	void setIsServer(bool b) { m_game->setIsServer(b); }

	int getLastLevelLoaded() const { return m_lastLevelLoaded; }
	bool hasLevelLoaded() const { return m_levelLoaded; }

	std::map<int, GameObject*> getDynamicObjectMap() { return m_game->getDynamicObjectMap(); };

	bool didPlayerHitBall()				const { return m_game->didPlayerHitBall(); }
	RayCastDataPacket getRayCastData()	const { return m_game->getRayCastData(); }

	void applyPlayerRaycast(const RayCastData& data);

protected:

	int m_lastLevelLoaded;
	bool m_levelLoaded;


	void initStates();
	void initTransitions();


	StateMachine* m_FSM;

	// ----- Game States ----- //
	State* m_introMenuState;
	State* m_pauseMenuState;
	State* m_endMenuState;
	State* m_gameState;

	// ----- Transitions ----- //
	std::vector<StateTransition*> m_levelTransition;
	StateTransition* m_lastTransition;
	// ----- misc ----- //
	float		m_dt;
	GameMenu*	m_gameMenu;
	Game*		m_game;
};

