#include "FilePrinter.h"



FilePrinter::FilePrinter():m_opened(false)
{
}


FilePrinter::~FilePrinter()
{
}

void FilePrinter::startPrinter(std::string filePath) {

	m_file.open(filePath.c_str());
	if (m_file.fail()) {

		std::string message = "File " + filePath + " does not exist!";
		return;
	}
	else {
		m_opened = true;
	}
}

void FilePrinter::endPrinter() {

	m_file.close();
	m_opened = false;
}

void FilePrinter::insertLine(std::string line)
{
	if (m_opened) {
		m_file << line.c_str() << std::endl;
	}
}


