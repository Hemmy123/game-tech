

#include "../../Common/Window.h"

#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/StateTransition.h"
#include "../CSC8503Common/State.h"

#include "../CSC8503Common/GameServer.h"
#include "../CSC8503Common/GameClient.h"

#include "../CSC8503Common/NavigationGrid.h"
#include "NetworkPlayer.h"
#include "TutorialGame.h"
#include "Game.h"
#include "NetworkedGame.h"
#include "GameStateManager.h"
#include "NetworkedGame.h"
#include "../CSC8503Common/PacketReceivers.h"


using namespace NCL;
using namespace CSC8503;


void MultiplayerGame() {


	// ----- Creating Window ----- //
	Window*w = Window::CreateGameWindow("CSC8503 Game technology!", 1280, 720);

	if (!w->HasInitialised()) { return;	}

	w->ShowOSPointer(false);
	w->LockMouseToWindow(true);

	// ----- Networked Game ----- //

	NetworkBase::Initialise();

	int port = NetworkBase::GetDefaultPort();
	BasicReceiver clientReceiver("Client");

	NetworkedGame* client = new NetworkedGame(&clientReceiver);



	//NetworkPlayer* player = new NetworkPlayer(game,1);
	client->connectToServer(127, 0, 0, 1, port);
	if(!client->isConnected()) {
		return;
	}
	string* msg = new string("Client says Hello!");

	StringPacket message(*msg);


	while (w->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		float dt = w->GetTimer()->GetTimeDelta() / 1000.0f;

		if (dt > 1.0f) {
			continue; //must have hit a breakpoint or something to have a 1 second frame time!
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_PRIOR)) {
			w->ShowConsole(true);
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_NEXT)) {
			w->ShowConsole(false);
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_P)) {

			// ----- Send test packets here! ----- //

			/*int id = 10;
			Vector3 position(1, 2, 3);
			Vector3 velocity(3.4, 2.1, 3.4);
			Quaternion orientation(1,1,1,1);

			GameObjectStatePacket gameStatePacket(id, position, velocity, orientation);
			game->sendPacket(gameStatePacket);
			std::cout << "send state packet" << std::endl;
			
			*/

			

		}

		client->update(dt);
		//std::this_thread::sleep_for(std::chrono::milliseconds(100));


		w->SetTitle("DogeBall FrameTime:" + std::to_string(1000.0f * dt));
	}





	Window::DestroyGameWindow();
	delete client;
	//delete player;
	NetworkBase::Destroy();
}

void SinglePlayerGame() {


	Window*w = Window::CreateGameWindow("CSC8503 Game technology!", 1280, 720);

	if (!w->HasInitialised()) {
		return;
	}


	w->ShowOSPointer(false);
	w->LockMouseToWindow(true);

	GameStateManager* gameManager = new GameStateManager();

	while (w->UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		float dt = w->GetTimer()->GetTimeDelta() / 1000.0f;

		if (dt > 1.0f) {
			continue; //must have hit a breakpoint or something to have a 1 second frame time!
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_PRIOR)) {
			w->ShowConsole(true);
		}
		if (Window::GetKeyboard()->KeyPressed(KEYBOARD_NEXT)) {
			w->ShowConsole(false);
		}

	
		gameManager->update(dt);
		w->SetTitle("DogeBall FrameTime:" + std::to_string(1000.0f * dt));
	}




	//delete gameManager;
	Window::DestroyGameWindow();

}


/*

The main function should look pretty familar to you!
We make a window, and then go into a while loop that repeatedly
runs our 'game' until we press escape. Instead of making a 'renderer'
and updating it, we instead make a whole game, and repeatedly update that,
instead. 

This time, we've added some extra functionality to the window class - we can
hide or show the 

*/
int main() {

	
	bool playGame = true;

	std::cout << "Would you like to play single or multiplayer?" << std::endl;
	std::cout << "1) Single Player" << std::endl;
	std::cout << "2) Multiplayer" << std::endl;

	int choice = 0;
	std::cin >> choice;

	if(choice == 1) {
		SinglePlayerGame();
	}
	if(choice == 2) {
		MultiplayerGame();

	} else {
		return 0;
	}



	return 0;

}