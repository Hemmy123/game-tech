#pragma once
#include <iostream>
#include <fstream>

class FilePrinter
{
public:
	FilePrinter();
	~FilePrinter();


	void startPrinter(std::string filePath);
	void endPrinter();


	void insertLine(std::string line);
	bool isOpen() const { return m_opened; }


private:

	std::ofstream	m_file;

	bool m_opened;
};

