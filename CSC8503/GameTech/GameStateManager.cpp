#include "GameStateManager.h"



GameStateManager::GameStateManager()
{
	m_FSM				= new StateMachine();
	m_game				= new Game();
	m_gameMenu			= new GameMenu(m_game->getRenderer());
	m_lastTransition	= nullptr;
	
	m_lastLevelLoaded	= 0;
	m_levelLoaded = false;

	initStates();
	initTransitions();

}


GameStateManager::~GameStateManager()
{
	delete m_FSM;

}

void GameStateManager::update(float dt) {
	

	m_FSM->Update();

	m_levelLoaded = false;

	m_lastTransition = m_FSM->getLastTransition();
	if(m_lastTransition) {
		if(m_lastTransition->getName() == "Load_Level") {
			GenericTransition<int&, int>* last = (GenericTransition<int&, int>*)m_lastTransition;
			// reset or else the FSM will keep loading the new level
			m_gameMenu->setSelectedLevel(0);
			m_game->loadLevel((int)last->getDataB());
			m_lastLevelLoaded	= (int)last->getDataB();
			m_levelLoaded		= true;
		}
		m_FSM->clearLastTransition();
	}


	if (m_game->getLoadNewClientLevel()) {
		m_lastLevelLoaded = m_game->getCurrentLevel();
		m_levelLoaded = true;
		m_game->setLoadNewClientLevel(false);
	}

	m_dt = dt;

}

void GameStateManager::applyPlayerRaycast(const RayCastData& data) {

	m_game->applyRaycastToPlayer(data);
}

void GameStateManager::initStates() {
	
	float* dt = &m_dt;

	// ----- Intro Menu state ----- //
	StateFunc introMenuFunc = [](void* data)
	{
		GameMenu* menu = (GameMenu*)data;
		menu->displayMenu(MenuType::Intro);
	};

	GenericState* introMenuState = new GenericState(introMenuFunc, m_gameMenu);
	introMenuState->SetName("Intro Menu State");
	m_introMenuState = introMenuState;

	// ----- Pause Menu state ----- //
	StateFunc pauseMenuFunc = [](void* data)
	{
		GameMenu* menu = (GameMenu*)data;
		menu->displayMenu(MenuType::Pause);
	};

	GenericState* pauseMenuState = new GenericState(pauseMenuFunc, m_gameMenu);
	pauseMenuState->SetName("Pause Menu State");
	m_pauseMenuState = pauseMenuState;

	// ----- End Menu state ----- //
	StateFunc endMenu = [](void* data)
	{
		GameMenu* menu = (GameMenu*)data;
		menu->displayMenu(MenuType::End);
	};

	GenericState* endMenuState	 = new GenericState(endMenu, m_gameMenu);
	endMenuState->SetName("End Menu State");
	m_endMenuState = endMenuState;

	// ----- Game state----- //

	auto updateGame = [dt](void* data)
	{
		Game* game = (Game*)data;
		game->UpdateGame(*dt);
	};

	GameState* gameState = new GameState(updateGame, m_game);
	gameState->SetName("Game State");
	m_gameState = gameState;


	// ----- End State ----- //


	// ----- Pause State ----- //

	m_FSM->AddState(m_introMenuState);
	m_FSM->AddState(m_pauseMenuState);
	m_FSM->AddState(m_endMenuState);
	m_FSM->AddState(m_gameState);


}

void GameStateManager::initTransitions() {

	// ----- Level loading from menu ----- //

	GenericTransition<int&, int>* loadLevel1 =
		new GenericTransition<int&, int>(
			GenericTransition<int&, int>::EqualsTransition, *(m_gameMenu->getSelectedLevel()), 1, m_introMenuState, m_gameState
	);
	loadLevel1->setName("Load_Level");

	GenericTransition<int&, int>* loadLevel2 =
		new GenericTransition<int&, int>(
			GenericTransition<int&, int>::EqualsTransition, *(m_gameMenu->getSelectedLevel()), 2, m_introMenuState, m_gameState
			);
	loadLevel2->setName("Load_Level");


	GenericTransition<int&, int>* loadLevel3 =
		new GenericTransition<int&, int>(
			GenericTransition<int&, int>::EqualsTransition, *(m_gameMenu->getSelectedLevel()), 3, m_introMenuState, m_gameState
			);
	loadLevel3->setName("Load_Level");


	// ----- Pausing and unpausing ----- //

	GenericTransition<int&, int>* pause =
		new GenericTransition<int&, int>(
			GenericTransition<int&, int>::EqualsTransition, *(m_game->getOptionPressed()), (int)'m', m_gameState, m_pauseMenuState
			);
	pause->setName("Pause");



	GenericTransition<int&, int>* unpause =
		new GenericTransition<int&, int>(
			GenericTransition<int&, int>::EqualsTransition, *(m_gameMenu->getUnpaused()), (int)'u', m_pauseMenuState, m_gameState
			);
	unpause->setName("Unpause");

	// ----- Endgame transition ----- //

	GenericTransition<bool&, bool>* end =
		new GenericTransition<bool&, bool>(
			GenericTransition<bool&, bool>::EqualsTransition, *(m_game->getEndGame()), true, m_gameState, m_endMenuState
			);
	end->setName("End");




	m_FSM->AddTransition(loadLevel1);
	m_FSM->AddTransition(loadLevel2);
	m_FSM->AddTransition(loadLevel3);
	m_FSM->AddTransition(pause);
	m_FSM->AddTransition(unpause);
	m_FSM->AddTransition(end);

}
