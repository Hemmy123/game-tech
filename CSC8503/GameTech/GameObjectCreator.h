#pragma once
#include "../CSC8503Common/GameObject.h"
#include "../CSC8503Common/PlayerGObject.h"
#include "../CSC8503Common/DynamicGObject.h"
#include "../CSC8503Common/StaticGObject.h"
#include "../CSC8503Common/EnemyGObject.h"
#include "../CSC8503Common/SpinningGObject.h"
#include "OGLMesh.h"
#include "OGLTexture.h"
#include "OGLShader.h"
#include "../CSC8503Common/GameWorld.h"
#include "../CSC8503Common/PatrollingGObject.h"

using namespace NCL::CSC8503;
using namespace NCL;


class GameObjectCreator
{
public:
	GameObjectCreator();
	~GameObjectCreator();

	GameObject* createObject(GameObjectType type, GameObjectType objType);

	GameObject* createPlayer(Vector3 pos, float radius, float mass, GameObjectType objType);

	// Mainly for the floor and walls
	GameObject* createStaticBox(Vector3 pos, Vector3 halfSize, GameObjectType objType = GameObjectType::StaticBox, Quaternion rotation = Quaternion());
	
	// For moving OBB boxes
	GameObject* createDynamicBox(Vector3 pos, Vector3 halfSize,float mass, Quaternion rotation, GameObjectType objType);

	// A AABB that has a mass so it can move around, but not rotate
	GameObject* CreateAABBDynamicBox(Vector3 pos, Vector3 halfSize,float mass, GameObjectType objType);

	GameObject* createSpinningObject(Vector3 pos, Vector3 halfSize, float speed, RotationOrientation orientation, GameObjectType objType);

	GameObject* createEnemy(Vector3 pos, Vector3 halfSize, float mass);

	PatrollingGObject* createPatrollingObject(Vector3 pos, Vector3 halfSize, GameObjectType type, float mass);


	void InitialiseAssets();

private:

	OGLTexture* selectTexture(GameObjectType objectType);

	OGLMesh*	m_cubeMesh		= nullptr;
	OGLMesh*	m_sphereMesh	= nullptr;
	OGLShader*	m_basicShader	= nullptr;

	OGLTexture* m_basicTex	= nullptr;
	OGLTexture* m_robotTex	= nullptr;
	OGLTexture* m_floorTex = nullptr;
	OGLTexture* m_playerTex = nullptr;
	OGLTexture* m_wallTex	= nullptr;
	OGLTexture* m_goalTex	= nullptr;

	// load other textures here!
	OGLTexture* m_enemyTexture = nullptr;

};

