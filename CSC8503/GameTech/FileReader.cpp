#include "FileReader.h"
#include <string>
#include <algorithm>


FileReader::FileReader():m_opened(false)
{
}


FileReader::~FileReader()
{
	if(m_opened) {
		endReader();
	}
}

bool FileReader::startReader(std::string filePath) {
	m_file.open(filePath.c_str());
	if (!m_file.is_open()) {

		std::string message = "File " + filePath + " Could not be opened!";

		std::cout << message << std::endl;
		m_opened = false;
		
		return m_opened;
	}
	else {
		m_opened = true;
		return m_opened;
	}

}

void FileReader::endReader() {
	m_file.close();
	m_opened = false;

}

std::string FileReader::readLine() {
	std::string line;

	if(m_file.good()) {
		getline(m_file, line);
	}
	return line;

}

std::vector<std::string> FileReader::readRestOfFile() {

	std::vector<std::string> output;

	while (m_file.good()) {
		std::string line;
		getline(m_file, line);		// Puts the read in line into 'line'
		if (!(line.empty())) {
			output.push_back(line);
		}
	}
	return output;
}

void FileReader::stripCommas(std::string& st) {
	st.erase( std::remove(st.begin(), st.end(), ',') , st.end()); // strips commas
}
 