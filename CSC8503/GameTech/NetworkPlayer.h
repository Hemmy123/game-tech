

#pragma once
#include "..\CSC8503Common\GameObject.h"
#include "..\CSC8503Common\GameClient.h"
#include "NetworkedGame.h"

namespace NCL {
	namespace CSC8503 {
		class NetworkPlayer : public GameObject {
		public:
			NetworkPlayer(NetworkedGame* game, int num);
			~NetworkPlayer();

			void OnCollisionBegin(GameObject* otherObject) override;

			int GetPlayerNum() const {
				return playerNum;
			}

		protected:
			NetworkedGame* game;
			int playerNum;
		};
	}
}

