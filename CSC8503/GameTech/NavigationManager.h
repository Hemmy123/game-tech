#pragma once
#include "../CSC8503Common/NavigationGrid.h"
#include "../CSC8503Common/NavigationMap.h"
#include "../CSC8503Common/Debug.h"
#include "../CSC8503Common/ObjectNavigator.h"

using namespace NCL::CSC8503;
//using namespace NCL::Debug;


class NavigationManager
{
public:
	NavigationManager();
	~NavigationManager();

	void update();
	void loadNewNavigationGrid(std::string fileName);
	bool findPath(Vector3 startPos, Vector3 endPos);
	void displayPathfinding();

	void setObject(GameObject* obj);
	GameObject* getObject() const { return m_navigator.getObject(); }


	void clearNavigator() { m_navigator.clearAll(); };

private:

	int m_cubeScale = 10;
	bool m_pathFound;

	Vector3 m_startPos;
	Vector3 m_endPos;

	ObjectNavigator m_navigator;
	NavigationGrid* m_navigationGrid;
	NavigationPath m_navigationPath;
	std::vector<Vector3> m_vectorPath;

	

};

