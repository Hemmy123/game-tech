#include "GameObjectCreator.h"
#include "../../Common/TextureLoader.h"
#include "../CSC8503Common/SphereVolume.h"
#include "../CSC8503Common/AABBVolume.h"
#include "../CSC8503Common/OBBVolume.h"
#include "../CSC8503Common/PositionConstraint.h"


GameObjectCreator::GameObjectCreator()
{
	InitialiseAssets();
}


GameObjectCreator::~GameObjectCreator()
{
}

GameObject* GameObjectCreator::createObject(GameObjectType type, GameObjectType objType) {
	return nullptr;

}

GameObject* GameObjectCreator::createPlayer(Vector3 pos, float radius, float mass, GameObjectType objType) {
	PlayerGObject* player = new PlayerGObject();
	player->SetType(GameObjectType::Player);

	Vector3 sphereSize = Vector3(radius, radius, radius);
	SphereVolume* volume = new SphereVolume(radius);
	player->SetBoundingVolume((CollisionVolume*)volume);
	player->GetTransform().SetWorldScale(sphereSize);
	player->GetTransform().SetWorldPosition(pos);

	player->SetRenderObject(new RenderObject(&player->GetTransform(), m_sphereMesh, selectTexture(objType), m_basicShader));
	player->SetPhysicsObject(new PhysicsObject(&player->GetTransform(), player->GetBoundingVolume()));

	if (mass == 0.0f) {
		player->GetPhysicsObject()->SetInverseMass(0.0f);
	} else {
		player->GetPhysicsObject()->SetInverseMass(1.0f / mass);
	}
	
	player->GetPhysicsObject()->InitSphereInertia();

	return player;

}

GameObject* GameObjectCreator::createStaticBox(Vector3 pos, Vector3 halfSize, GameObjectType objType, Quaternion rotation) {
	StaticGObject* wall = new StaticGObject();
	wall->SetType(objType);


	if(rotation.IsAxisAligned()) {
		AABBVolume* volume = new AABBVolume(halfSize);
		wall->SetBoundingVolume((CollisionVolume*)volume);
	} else {
		OBBVolume* volume = new OBBVolume(halfSize);
		wall->SetBoundingVolume((CollisionVolume*)volume);
	}

	wall->GetTransform().SetWorldPosition(pos);
	wall->GetTransform().SetWorldScale(halfSize);

	wall->SetRenderObject(new RenderObject(&wall->GetTransform(), m_cubeMesh, selectTexture(objType), m_basicShader));
	wall->SetPhysicsObject(new PhysicsObject(&wall->GetTransform(), wall->GetBoundingVolume()));

	wall->GetPhysicsObject()->SetInverseMass(0);
	wall->GetPhysicsObject()->InitCubeInertia();

	return wall;

}

GameObject* GameObjectCreator::createDynamicBox(Vector3 pos, Vector3 halfSize, float mass,  Quaternion rotation, GameObjectType objType) {
	DynamicGObject* box = new DynamicGObject();
	box->SetType(objType);

	// should really be OBB... 
	//AABBVolume* volume = new AABBVolume(halfSize);
	OBBVolume* volume = new OBBVolume(halfSize);

	box->SetBoundingVolume((CollisionVolume*)volume);

	box->GetTransform().SetWorldPosition(pos);
	box->GetTransform().SetWorldScale(halfSize);
	box->GetTransform().SetLocalOrientation(rotation);

	box->SetRenderObject(new RenderObject(&box->GetTransform(), m_cubeMesh, selectTexture(objType), m_basicShader));
	box->SetPhysicsObject(new PhysicsObject(&box->GetTransform(), box->GetBoundingVolume()));


	if (mass == 0.0f) {
		box->GetPhysicsObject()->SetInverseMass(0.0f);
	}
	else {
		box->GetPhysicsObject()->SetInverseMass(1.0f / mass);
	}

	box->GetPhysicsObject()->InitCubeInertia();

	return box;

}

GameObject* GameObjectCreator::CreateAABBDynamicBox(Vector3 pos, Vector3 halfSize, float mass, GameObjectType objType) {

	StaticGObject* box = new StaticGObject();
	box->SetType(objType);

	AABBVolume* volume = new AABBVolume(halfSize);
	box->SetBoundingVolume((CollisionVolume*)volume);

	box->GetTransform().SetWorldPosition(pos);
	box->GetTransform().SetWorldScale(halfSize);

	box->SetRenderObject(new RenderObject(&box->GetTransform(), m_cubeMesh, selectTexture(objType), m_basicShader));
	box->SetPhysicsObject(new PhysicsObject(&box->GetTransform(), box->GetBoundingVolume()));

	if (mass == 0.0f) {
		box->GetPhysicsObject()->SetInverseMass(0.0f);
	}
	else {
		box->GetPhysicsObject()->SetInverseMass(1.0f / mass);
	}	
	box->GetPhysicsObject()->InitCubeInertia();

	return box;
}

GameObject* GameObjectCreator::createSpinningObject(Vector3 pos, Vector3 halfSize, float speed,
	RotationOrientation orientation, GameObjectType objType) {

	SpinningGObject* spinObj = new SpinningGObject(speed, orientation);


	spinObj->SetType(objType);
	OBBVolume* volume = new OBBVolume(halfSize);

	Quaternion defaultRot;

	spinObj->SetBoundingVolume((CollisionVolume*)volume);
	spinObj->GetTransform().SetWorldPosition(pos);
	spinObj->GetTransform().SetWorldScale(halfSize);
	spinObj->GetTransform().SetLocalOrientation(defaultRot);

	spinObj->SetRenderObject(new RenderObject(&spinObj->GetTransform(), m_cubeMesh, selectTexture(objType), m_basicShader));
	spinObj->SetPhysicsObject(new PhysicsObject(&spinObj->GetTransform(), spinObj->GetBoundingVolume()));

	spinObj->GetPhysicsObject()->SetInverseMass(0.0f);
	spinObj->GetPhysicsObject()->InitCubeInertia();

	return spinObj;
}

GameObject* GameObjectCreator::createEnemy(Vector3 pos, Vector3 halfSize, float mass) {
	EnemyGObject* enemyGObject = new EnemyGObject();

	enemyGObject->SetType(GameObjectType::Enemy);

	AABBVolume* volume = new AABBVolume(halfSize);
	enemyGObject->SetBoundingVolume((CollisionVolume*)volume);

	enemyGObject->GetTransform().SetWorldPosition(pos);
	enemyGObject->GetTransform().SetWorldScale(halfSize);

	enemyGObject->SetRenderObject(new RenderObject(&enemyGObject->GetTransform(), m_cubeMesh, selectTexture(GameObjectType::Enemy), m_basicShader));
	enemyGObject->SetPhysicsObject(new PhysicsObject(&enemyGObject->GetTransform(), enemyGObject->GetBoundingVolume()));

	if (mass == 0.0f) {
		enemyGObject->GetPhysicsObject()->SetInverseMass(0.0f);
	}
	else {
		enemyGObject->GetPhysicsObject()->SetInverseMass(1.0f / mass);
	}

	enemyGObject->GetPhysicsObject()->InitCubeInertia();

	return enemyGObject;

		

}

PatrollingGObject* GameObjectCreator::createPatrollingObject(Vector3 pos, Vector3 halfSize, GameObjectType type, float mass) {
	PatrollingGObject* patrollingObj = new PatrollingGObject();

	patrollingObj->SetType(type);

	AABBVolume* volume = new AABBVolume(halfSize);
	patrollingObj->SetBoundingVolume((CollisionVolume*)volume);

	patrollingObj->GetTransform().SetWorldPosition(pos);
	patrollingObj->GetTransform().SetWorldScale(halfSize);

	patrollingObj->SetRenderObject(new RenderObject(&patrollingObj->GetTransform(), m_cubeMesh, selectTexture(GameObjectType::Enemy), m_basicShader));
	patrollingObj->SetPhysicsObject(new PhysicsObject(&patrollingObj->GetTransform(), patrollingObj->GetBoundingVolume()));

	if (mass == 0.0f) {
		patrollingObj->GetPhysicsObject()->SetInverseMass(0.0f);
	}
	else {
		patrollingObj->GetPhysicsObject()->SetInverseMass(1.0f / mass);
	}

	patrollingObj->GetPhysicsObject()->InitCubeInertia();

	return patrollingObj;

}


void GameObjectCreator::InitialiseAssets() {
	m_cubeMesh = new OGLMesh("cube.msh");
	m_cubeMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	m_cubeMesh->UploadToGPU();

	m_sphereMesh = new OGLMesh("sphere.msh");
	m_sphereMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	m_sphereMesh->UploadToGPU();

	m_basicTex	= (OGLTexture*)TextureLoader::LoadAPITexture("checkerboard.png");
	m_robotTex	= (OGLTexture*)TextureLoader::LoadAPITexture("robot.png");
	m_floorTex	= (OGLTexture*)TextureLoader::LoadAPITexture("grass.png");
	m_playerTex	= (OGLTexture*)TextureLoader::LoadAPITexture("pikachu.png");
	m_wallTex	= (OGLTexture*)TextureLoader::LoadAPITexture("wood.png");
	m_goalTex = (OGLTexture*)TextureLoader::LoadAPITexture("doge.png");


	m_basicShader = new OGLShader("GameTechVert.glsl", "GameTechFrag.glsl");

}

OGLTexture* GameObjectCreator::selectTexture(GameObjectType objectType) {

	switch(objectType) {
	case GameObjectType::Player: { return m_playerTex; }
	case GameObjectType::Enemy: { return m_robotTex; }
	case GameObjectType::Goal: { return m_goalTex; }
	case GameObjectType::Windmill: { return m_wallTex; }

	case GameObjectType::StaticBox: { return m_wallTex; }
	case GameObjectType::Mace: { return m_wallTex; }
	case GameObjectType::Floor: { return m_floorTex; }

	case GameObjectType::DynamicBox: {
		return m_wallTex;
	}
	case GameObjectType::AABBDynamicBoxes: {
		return m_wallTex;
	}
	}
}
