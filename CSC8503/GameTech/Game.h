#pragma once
#include "TutorialGame.h"
#include "GameObjectCreator.h"
#include "Level.h"
#include "LevelLoader.h"
#include "../CSC8503Common/NavigationGrid.h"
#include "NavigationManager.h"
#include "../CSC8503Common/StateMachine.h"


class Game : public TutorialGame{
public:
	Game(int id = 0);
	~Game();

	void initScores();

	void loadLevel(int level);


	void movePlayer(PlayerGObject* playerObj);

	virtual void UpdateGame(float dt) override;

	void applyRaycastToPlayer(const RayCastData& data);

	bool* getEndGame() {
		return &m_finishedGame;
	};
	int* getOptionPressed() { return &m_pausedPressed; };

	void clearGameObjects();

	bool getScoreUpdated() const { return m_scoreUpdated; }

	int getScore() const { return m_score;  }

	bool getLevelFinished() { return m_finishedLevel; }
	int getCurrentLevel() { return m_currentLevel; }

	void displayerHighScores(std::map<int, int> highScores);

	void updateDynamicObject(int id, const GameObjectState& newState);

	void setIsServer(bool b) { m_isServer = b; }
	void setMultiplayer(bool b) { m_multiplayer = b; }

	bool getLoadNewClientLevel() { return m_loadNewClientLevel; }
	void setLoadNewClientLevel(bool b) { m_loadNewClientLevel = b; }

	std::map<int, GameObject*> getDynamicObjectMap() { return m_dynamicObjectMap; };

	bool didPlayerHitBall() const { return m_playerRaycasted; }
	RayCastDataPacket getRayCastData() const { return m_playerRaycastData; }


protected:


	// ----- Networking stuff----- //
	// Added in abit late minute, so this should
	// all really be inside of a child class or 
	// something

	bool m_isServer;
	bool m_multiplayer;

	bool m_playerRaycasted;
	RayCastDataPacket m_playerRaycastData;

	// ----- Level Loading ----- //

	void readLevelFile(std::string filePath);

	void loadLevelObj(Level* level);

	void resetLevel();

	void resetGoalReached();




	// ----- Specific Objects ----- //
	std::vector<PlayerGObject*>		m_playerObjects;
	std::vector<EnemyGObject*>		m_enemyObjects;
	std::vector<GameObject*>		m_goalObjects;
	std::vector<SpinningGObject*>	m_spinningObjects;
	std::vector<PatrollingGObject*> m_maces;
	std::vector<GameObject*>		m_dynamicObjects;

	std::map<int, GameObject*> m_dynamicObjectMap;

	// ----- Score Tracking----- //
	int m_score;

	void printScore();

	// ----- Game logic ----- //
	int m_scoreUpdated;

	int		m_currentLevel;
	int		m_lastLevel;
	bool	m_finishedLevel;
	bool	m_loadNewClientLevel;
	bool	m_finishedGame;

	int		m_pausedPressed;

	float	m_killPlane = 0;

	int		m_cubeScale;
	float	m_patrolSpeed;

	void respawnPlayer(PlayerGObject* player);
	void resetPlayerScore();
	void updateGameKeys();

	void checkRespawn();
	void spinObjects();
	void displayText();

	void patrolMace();


	// ----- Level loading ----- //
	GameObjectCreator*	m_objectCreator;
	LevelLoader* 		m_loader;
	NavigationManager	m_navigationManager;
	StateMachine		m_stateMachine;

};

