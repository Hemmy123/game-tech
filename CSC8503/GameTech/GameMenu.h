#pragma once
#include "../../Common/Window.h"
#include "../CSC8503Common/Constraint.h"
#include "GameTechRenderer.h"

using namespace NCL::CSC8503;
using namespace NCL;

enum class MenuType {
	Intro,
	Pause,
	End
};

class GameMenu
{
public:
	GameMenu(GameTechRenderer * renderer);
	~GameMenu();
	
	
	void update();

	


	int* getSelectedLevel() { return &m_selectedLevel; };
	int* getUnpaused() { return &m_unpausedPressed; };
	void setSelectedLevel(int i) { m_selectedLevel = i; }

	void displayMenu(MenuType menu);

	void setType(MenuType type) { m_type = type; }
	
private:

	void introMenu();
	void pauseMenu();
	void endMenu();


	MenuType m_type;

	int m_selectedLevel;
	int m_unpausedPressed;
	GameTechRenderer* m_renderer;
};

