#pragma once
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <vector>
class FileReader
{
public:
	FileReader();
	~FileReader();

	bool startReader(std::string filePath);
	void endReader();

	std::string readLine();

	bool isOpen() const { return m_opened; }

	std::vector<std::string> readRestOfFile();

	void stripCommas(std::string & string);
private:

	std::ifstream	m_file;

	bool m_opened;
};

