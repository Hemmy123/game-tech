#pragma once
#include "../CSC8503Common/GameObject.h"
#include <vector>
#include "LevelLoader.h"

using namespace NCL::CSC8503;
class Level
{
public:
	Level();
	~Level();

	/// Reads a level from a text file.
	int readLevel();

	std::vector<GameObject*> getGameObjects() const { return m_gameObjects; }
	void setGameObjects(std::vector<GameObject*> gameObjects) { m_gameObjects = gameObjects; }

private:

	std::vector<GameObject*> m_gameObjects;
	//LevelLoader m_levelLoader;
	
};

